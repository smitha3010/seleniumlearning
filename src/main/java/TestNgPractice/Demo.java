package TestNgPractice;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Demo {
	
	
	@Test(dataProvider="loginData")
	public void testLogin(String username,String password) {
		
		System.out.println("User having username: "+username+" and password: "+password+" got logged in");

	}
	
	@DataProvider(name="loginData")
	public String[][] loginData() {
		String [][] obj = {{"Adminuser","APassword"},{"TeacherUsername","TPassword"},
				{"StudentUsername","SPassword"}};
		return obj;
		


	}

}
