package SeleniumPgmPractise;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SnapsPractise {

	public static void main(String[] args) throws IOException, InterruptedException {
		
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/Window.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		TakesScreenshot scrShot=((TakesScreenshot)driver);
		
		//WebElement eleHome = driver.findElementById("home");
		
	File source=scrShot.getScreenshotAs(OutputType.FILE);
	
	File target=new File("./snaps/webtable1.png");
	
	FileUtils.copyFile(source, target);
	
	Thread.sleep(5000);
	driver.close();

	}

}
