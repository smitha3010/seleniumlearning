package SeleniumPgmPractise;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebTable {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/table.html");
		driver.manage().window().maximize();

		List<WebElement> allRows = driver.findElementsByXPath("//table[@id='table_id']/tbody/tr");
		int rowCount = allRows.size();

		for (int i = 2; i <= rowCount; i++) {

			List<WebElement> allData = driver.findElementsByXPath("//table[@id='table_id']/tbody/tr[" + i + "]/td");
			int colCount = allData.size();
			for (int j = 1; j <= colCount; j++) {
				String text = driver.findElementByXPath("//table[@id='table_id']/tbody/tr[" + i + "]/td[" + j + "]")
						.getText();
				System.out.println(text);
				
				if(text.equals("Gopi"))
				{
					System.out.println("Row Num: "+i);
					System.out.println("Col Num: "+j);
				}

			}

		}
		driver.close();

	}

}
