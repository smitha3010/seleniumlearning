package javaProgramsInterviewPractise;

public class StringPalindrome {

	public static void main(String[] args) {
		String str="madam";
		String rev="";
		
		for (int i = str.length()-1; i >=0; i--) {
			
			rev=rev+str.charAt(i);
			
		}
		if(str.equalsIgnoreCase(rev))
		{
			System.out.println("is palindrome");
			
		}else
		{
			System.out.println("is not palindrome");
		}

	}

}
