package javaProgramsInterviewPractise;

public class ReverseStringPractise {

	public static void main(String[] args) {

		String input = "sg software testing";

		// convert String to character array
		// by using toCharArray
//	        char[] try1 = input.toCharArray();
//	 
//	        for (int i = try1.length - 1; i >= 0; i--) {
//	        	System.out.print(try1[i]);
//	        	
//	        }

//		 for (int i = input.length()-1; i>=0; i--) {
//			 System.out.print(input.charAt(i));
//			
//		}

		StringBuilder sbr = new StringBuilder(input);
		sbr.reverse();
		System.out.println(sbr);

	}

}
