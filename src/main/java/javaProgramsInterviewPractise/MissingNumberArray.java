package javaProgramsInterviewPractise;

public class MissingNumberArray {

	public static void main(String[] args) {
		int arr[]= {1,3,4,5,6,9};
		
		int length = arr[arr.length-1];
		
		int count =0;
		
		for (int i = 0; i < length; i++) {
			
			if(i==arr[count])
			{
				count++;
			}
			else 
				System.out.println(i);
			
		}
		

	}

}
