package week8.day1;

public class FinallyBlock {

	public static void main(String[] args) {
		int x = 10;
		int y = 5;
		int z = 0;

		int[] num = { 10, 20, 30 };

		try {
			z = x / y;
			System.out.println(num[3]);
		} finally {
			System.out.println("finally block");
		}


		System.out.println(z);

		System.out.println("End ofthe program");

	}

}
