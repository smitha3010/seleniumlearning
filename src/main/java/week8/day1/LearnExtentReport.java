package week8.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtentReport {

	public static void main(String[] args) throws IOException {
		//Step1 : create physical html file
		
		ExtentHtmlReporter reporter = new ExtentHtmlReporter("./reports/result.html");
		
		
		//to maintain history of execution
		reporter.setAppendExisting(true);
		
		//Step2: Create the extent report to capture the data
		
		ExtentReports extent = new ExtentReports();
		
		//Step3 : Attach the actual data to the physical file
		extent.attachReporter(reporter);
		
		
		
		//steps to create test case and setup details
		ExtentTest test = extent.createTest("LoginAndLogout","Login test for leaftaps application");
		test.assignAuthor("Smitha");
		test.assignCategory("Smoke");
		
		//update the status for test steps
		test.pass("Enter username as demosalesmanager", MediaEntityBuilder.createScreenCaptureFromPath(".././snaps/flights.png").build());
		test.pass("Enter password as crmsfa");
		
		test.pass("click login button");
		
		//last mandatory step
		extent.flush();
		
		
		
		
		

	}

}

