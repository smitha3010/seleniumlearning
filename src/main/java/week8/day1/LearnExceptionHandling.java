package week8.day1;

public class LearnExceptionHandling {

	public static void main(String[] args) {
		int x = 10;
		int y = 0;
		int z = 0;

		int[] num = { 10, 20, 30 };

		try {
			z = x / y;
			System.out.println(num[3]);
		} catch (ArithmeticException e) {

			System.out.println("exception becoz of division by zero");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}

//		try {
//			
//		} catch (StaleElementReferenceException e) {
//			driver.navigate.to.refresh();
//		}

		System.out.println(z);

		System.out.println("End ofthe program");

	}

}
