package week2.day1.assignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumAssignmentOne {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/login");
		driver.manage().window().maximize();
		WebElement userName = driver.findElementById("username");
		userName.sendKeys("DemoSalesManager");
		driver.findElementByName("PASSWORD").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();

		System.out.println("The title is : " + driver.getTitle());

		if (driver.getTitle().contains("TestLeaf")) {
			System.out.println("Title matched");
		} else {
			System.out.println("Title not matched");
		}
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		
		driver.findElementByXPath("//input[@id='createLeadForm_companyName']").sendKeys("Company name Testing");
		driver.findElementByXPath("//input[@id='createLeadForm_firstName']").sendKeys("Smitha FN");
		driver.findElementByXPath("//input[@id='createLeadForm_lastName']").sendKeys("LN");
		
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dropDown1 = new Select(source);
		dropDown1.selectByVisibleText("Direct Mail");
		
		WebElement Market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dropDown2 = new Select(Market);
		dropDown2.selectByVisibleText("Pay Per Click Advertising");
		
		driver.findElementByXPath("//input[@id='createLeadForm_firstNameLocal']").sendKeys("test1");
		
		driver.findElementByXPath(" //input[@id='createLeadForm_lastNameLocal']").sendKeys("test2");
		
		driver.findElementByXPath("//input[@id='createLeadForm_personalTitle'] ").sendKeys("test3");	
		
		driver.findElementByXPath("//input[@id='createLeadForm_birthDate'] ").sendKeys("12/10/2020");
		
		driver.findElementByXPath("//input[@id='createLeadForm_generalProfTitle']").sendKeys("test4");
		
		driver.findElementByXPath("//input[@id='createLeadForm_departmentName']").sendKeys("test5");
		
		driver.findElementByXPath("//input[@id='createLeadForm_annualRevenue']").sendKeys("1000000");
		
		WebElement Currency = driver.findElementById("createLeadForm_currencyUomId");
		Select dropDown3 = new Select(Currency);
		dropDown3.selectByVisibleText("INR - Indian Rupee");
		
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select dropDown4 = new Select(industry);
		dropDown4.selectByVisibleText("Computer Software");
		
		driver.findElementByXPath("//input[@id='createLeadForm_numberEmployees']").sendKeys("1000");
		
				
		WebElement ownership = driver.findElementById("createLeadForm_ownershipEnumId");
		Select dropDown5 = new Select(ownership);
		dropDown5.selectByVisibleText("Partnership");
		
		driver.findElementByXPath("//input[@id='createLeadForm_sicCode']").sendKeys("5600");
		
		driver.findElementByXPath("//input[@id='createLeadForm_tickerSymbol']").sendKeys("lotus");
		
		driver.findElementByXPath("//textarea[@id='createLeadForm_description']").sendKeys("test7");
		
		driver.findElementByXPath("//textarea[@id='createLeadForm_importantNote']").sendKeys("test8");
		
		driver.findElementByXPath("//input[@id='createLeadForm_primaryPhoneAreaCode']").sendKeys("6999");
		
		driver.findElementByXPath("//input[@id='createLeadForm_primaryPhoneNumber']").sendKeys("8888888888");
		
		driver.findElementByXPath("//input[@id='createLeadForm_primaryPhoneExtension']").sendKeys("91");
		
		driver.findElementByXPath("//input[@id='createLeadForm_primaryPhoneAskForName']").sendKeys("test9");
		
		driver.findElementByXPath("//input[@id='createLeadForm_primaryEmail']").sendKeys("smi@gmail.com");
		
		driver.findElementByXPath("//input[@id='createLeadForm_primaryWebUrl']").sendKeys("https://smi");
		
		driver.findElementByXPath("//input[@id='createLeadForm_generalToName']").sendKeys("test10");
		
		driver.findElementByXPath("//input[@id='createLeadForm_generalAttnName']").sendKeys("test11");
		
		driver.findElementByXPath("//input[@id='createLeadForm_generalAddress1']").sendKeys("test12");
		
		driver.findElementByXPath("//input[@id='createLeadForm_generalAddress2']").sendKeys("test13");
		
		driver.findElementByXPath("//input[@id='createLeadForm_generalCity']").sendKeys("test14");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select dropDown6 = new Select(state);
		dropDown6.selectByVisibleText("Alaska");
		
		driver.findElementByXPath("//input[@id='createLeadForm_generalPostalCode']").sendKeys("56666");
		
		WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select dropDown7 = new Select(country);
		dropDown7.selectByVisibleText("India");
		
		driver.findElementByXPath("//input[@id='createLeadForm_generalPostalCodeExt']").sendKeys("45");
		
		driver.findElementByName("submitButton").click();
		
		driver.findElementByXPath("//a[text()='Duplicate Lead']").click();
		driver.findElementByXPath("//input[@id='createLeadForm_companyName']").clear();
		driver.findElementByXPath("//input[@id='createLeadForm_companyName']").sendKeys("NewCompanyName");
		
		driver.findElementByName("submitButton").click();
		
		WebElement cname = driver.findElementById("viewLead_companyName_sp");
		
		String company=cname.getText();
		
		if(company.contains("NewCompanyName")) {
			System.out.println("sucessfully updated");
		}else {
			System.out.println("not updated");
		}
	
		driver.close();	
		

	}

}
