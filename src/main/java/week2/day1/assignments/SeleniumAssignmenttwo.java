package week2.day1.assignments;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumAssignmenttwo {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.google.com");
		driver.manage().window().maximize();
		boolean displayed = driver.findElementByName("q").isDisplayed();
		if (displayed) {
			System.out.println("the search field is displayed");
			driver.findElementByName("q").sendKeys("Selenium", Keys.ENTER);
		}

	}

}
