package week2.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearnToInteractWithDropDown {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();

		ChromeDriver driver = new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps/control/main");

		driver.manage().window().maximize();

		// Thread.sleep(5000);

		WebElement userName = driver.findElementById("username");

		userName.sendKeys("DemoCSR");

		driver.findElementByName("PASSWORD").sendKeys("crmsfa");
		// driver.findElementByName("PASSWORD").clear();

		driver.findElementByClassName("decorativeSubmit").click();

		driver.findElementByLinkText("CRM/SFA").click();

		driver.findElementByLinkText("Leads").click();

		driver.findElementByLinkText("Create Lead").click();

		WebElement source = driver.findElementById("createLeadForm_dataSourceId");

		Select dropDown = new Select(source);

		// dropDown.selectByVisibleText("Direct Mail");
		// dropDown.selectByValue("LEAD_DIRECTMAIL");
		dropDown.selectByIndex(1);
		
		WebElement industry=driver.findElementById("createLeadForm_industryEnumId");
		
		Select dropDown1 = new Select(industry);
		
		dropDown1.selectByValue("IND_HEALTH_CARE");
		
		
		driver.findElementById("createLeadForm_marketingCampaignId").sendKeys("Automobile");
		

	}

}
