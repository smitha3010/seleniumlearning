package week2.day1;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LaunchApplication {

	public static void main(String[] args) throws InterruptedException {

		// precondition:set up the driver path
		WebDriverManager.chromedriver().setup();

		// Step1: Open ChromeBrowser
		ChromeDriver driver = new ChromeDriver();

		driver.get("http://leaftaps.com/opentaps/control/main");

		// to maximize the window
		driver.manage().window().maximize();
		
		Thread.sleep(5000);
		
		//*[@id="username"]

		driver.close();

	}

}
