package week1.day1;

public class Mobile {
	
	String mobileModel="Oneplus";
	int mobileWeight=15;
	boolean isFullCharge=true;
	double mobileCost=50000.343435;
	
	public void makeCall()
	{
		System.out.println("Making Call");
	}
  
	public void sendMsg()
	{
		System.out.println("Sending message");
	}
	
	public static void main(String[] args) {
		
		Mobile object=new Mobile();
		
		object.makeCall();
		
		object.sendMsg();
		
		System.out.println("My mobile model is:"+object.mobileModel);
		
		System.out.println("Mymobile weight is :"+object.mobileWeight);
		
		System.out.println("is my mobile fully charged? :"+object.isFullCharge);
		
		System.out.println("My mobile cost is  :"+object.mobileCost);
		
		
		
	}

}
