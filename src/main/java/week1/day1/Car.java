package week1.day1;

public class Car {
	
	String bodyColor="Blue";
	int numWheels=4;
	int distanceTravelled=45000;
	long ownerPhNo=7259186253L;
	boolean isServiced=true;
	char fuelType='D';
	double carPrice=90000.5074747478;
	float fuelCapacity=30.5F;
	String regNumber="KA035774";
	
	
	
	public void driveCar()
	{
		System.out.println("driving a car");
		System.out.println(fuelCapacity);
	}
	
	
	public String getCarColor() {
	//String color=bodyColor;
	return bodyColor;

	}
	
	public boolean checkServiceStatus() {
		boolean status=isServiced;
		return status;

	}

	public static void main(String[] args) {
		
		Car myCar=new Car();
		
		myCar.driveCar();
		
		System.out.println(myCar.ownerPhNo);//global variable
		System.out.println("Car price using global variable :"+myCar.carPrice);
		
		double price=myCar.carPrice;
		System.out.println("Car price using local variable :"+price);
		
		String carColor=myCar.getCarColor();
		System.out.println(carColor);
		
		boolean serviceStatus=myCar.checkServiceStatus();
		System.out.println(serviceStatus);
		
		
		
		
		
	}
}
