package week1.day2;

public class LearControlStatements {

	public static void main(String[] args) {
		
		for(int i=10;i>0;i--)
		{
			System.out.println(i);
		}
		
		System.out.println("==============================");
		
		for (int i = 1; i <=5; i++) {
			
			if(i==3)
			{
				System.out.println("three");
				//continue;
				break;
			}
			//System.out.println(i);
		}
	}

}
