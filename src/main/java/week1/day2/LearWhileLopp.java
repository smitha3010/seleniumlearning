package week1.day2;

public class LearWhileLopp {

	public static void main(String[] args) {
		
		int input =5;
		
		while(input<10)
		{
			System.out.println(input);
			
			input=input+1;
		}
		
		System.out.println("==============");
		
		do {
			System.out.println(input);
			
			input=input+1;
		}while(input<10);
	}

}
