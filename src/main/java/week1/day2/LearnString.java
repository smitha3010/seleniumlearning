package week1.day2;

public class LearnString {

	public static void main(String[] args) {
		//declare using literal;it is case sensitive
		String str1="welcome1234";//sequence characters;all the characters in the string has index
		//index starts with 0
		//last character index is length()-1
     	String str2="Welcome";
//		//decalre using keyword
//		String str3 = new String("welcome");
//		
//		
//		System.out.println(str1==str2);
//		System.out.println(str1==str3);
     	System.out.println(str1.length()==str2.length());
     	
     	//to compare the values
     	
     	System.out.println(str1.equals(str2));
     	
     	//equalignorecase
     	
     	System.out.println(str1.equalsIgnoreCase(str2));
     	
     	//method to count the characters with string
     	
     	System.out.println(str1.length());
     	
     	//method to get the single character
     	
     	System.out.println(str1.charAt(str1.length()-1));
     	
     	for(int i=0;i<str1.length();i++)
     	{
     		System.out.println(str1.charAt(i));
     	}
     	for(int i=str1.length()-1;i>=0;i--)
     	{
     		System.out.println(str1.charAt(i));
     	}
     	
     	char[] chars = str1.toCharArray();
     	
     	for(int i=0;i<chars.length;i++)
     	{
     		System.out.println(chars[i]);
     	     		
     	}
		
     	System.out.println("=================================");
     	//Substring or partial string 
     	
     	System.out.println(str1.substring(3));
     	System.out.println(str1.substring(0, 4));
     	
     	System.out.println("====================================");
     	
     	System.out.println(str1.replace('e', 'x'));
     	System.out.println(str1.replace("e", ""));
     	System.out.println(str1.replaceAll("[0-9]", " "));
		
				

	}

}
