package week1.day2;

public class Calculator {
	
	public int add(int num1,int num2,int num3)
	{
		 int sum=num1+num2+num3;
		System.out.println("addition is : "+ sum);
		 return sum ;
			
	}
	public int sub(int num1,int num2)
	{
		 int sum=num1-num2;
		System.out.println("substraction is : "+ sum);
		 return sum ;
		
	}
	
	public double mul(double num1,double num2)
	{
		double sum =num1*num2;
		System.out.println("multiplication is : "+ sum);
		return sum;
		
	}
	
	public float div(float num1,float num2)
	{
		float sum=num1/num2;
		System.out.println("division is  : "+ sum);
		return sum;
	}
	

	public static void main(String[] args) {
		
		Calculator myCal=new Calculator();
		myCal.add(5,13,14);
		myCal.sub(10, 5);
		myCal.mul(900.00454,500.9089788);
		myCal.div(30.23F, 5.02F);
		

	}

}
