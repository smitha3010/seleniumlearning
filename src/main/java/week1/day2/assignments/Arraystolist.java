package week1.day2.assignments;

import java.util.Arrays;
import java.util.List;

public class Arraystolist {

	public static void main(String[] args) {
		String arr1[] = {"A", "B", "C", "D", "E"};
		List<String> list1=Arrays.asList(arr1);
		System.out.println(list1);
		
		Integer arr2[] = {1,2,3,4,5,6};
		List<Integer> list2 = Arrays.asList(arr2);
		
		System.out.println(list2);
		

	}

}
