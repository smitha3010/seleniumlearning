package week1.day2.assignments;

import java.util.Arrays;

public class Twodto1d {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int arr[][] = {{10, 20}, {30, 40}};  //2x2
		int rows = arr.length;
		int column = arr[0].length;
		
		
		
		//Resultant 1 D array to store the 2 D array
		int res[] = new int[rows * column];
		
		//Assign the 2 D array values to 1 D array as below
		int x = 0;
		for(int i=0; i<arr.length; i++)
		{
			for(int j=0; j<arr[i].length; j++)
			{
				res[x] = arr[i][j];
				x++;
			}
		}
		
		
		//Display the result
		System.out.println(Arrays.toString(res));
	}

}
