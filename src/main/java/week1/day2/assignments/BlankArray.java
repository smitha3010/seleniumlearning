package week1.day2.assignments;

public class BlankArray {

	public static void main(String[] args) {
		int Size = 5;
		int array[] = new int[Size];
		for (int i = 0; i < Size; i++) {
			array[i]=i+1;
			System.out.println("The value stored in array on index "+i+"is:"+array[i]);
			
			System.out.println("=+++++++++++++++++++++++++++++++++++=");
//			int arr[] = {};
//			System.out.println(arr.length);
//			
//			String str[] = {};
//			System.out.println(str.length);
//			
//			char ch[] = {};
//			System.out.println(ch.length);
//			
		}
	}

}
