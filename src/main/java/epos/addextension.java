package epos;

import java.io.File;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class addextension {

	public static void main(String[] args) {
		
		ChromeOptions option = new ChromeOptions();
		option.addExtensions(new File("./data/Tab Saver 2.4.0.0.crx"));
		
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver(option);
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		

	}

}
