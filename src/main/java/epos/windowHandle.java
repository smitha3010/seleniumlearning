package epos;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class windowHandle {

	public static void main(String[] args) throws IOException {
		WebDriverManager.chromedriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/Window.html");

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		String s=driver.getPageSource();
		System.out.println(s);

//		String firstWindow = driver.getWindowHandle();
//    	System.out.println(firstWindow);
//		driver.findElementByXPath("//*[@id='home']").click();
//
//		Set<String> ele = driver.getWindowHandles();
//		
//
////	for (String gss : ele) {
////		System.out.println(gss);
////		
////	}
////	
//		System.out.println("-----------------------");
//
//		List<String> ele1 = new ArrayList<String>(ele);
//		String SecondWindow = null;
//
//		for (String eachelement : ele1) {
//
//			if (!eachelement.contentEquals(firstWindow)) {
//				SecondWindow = eachelement;
//
//			}
//		}
//
//		// String wind= ele1.get(SecondWindow);
//		driver.switchTo().window(SecondWindow);
//		driver.findElementByXPath("//*[text()='Button']").click();
//		
//	File source=driver.getScreenshotAs(OutputType.FILE);
//	
//	File target = new File ("./snaps/leaftap.png");
//	
//	FileUtils.copyFile(source, target);
//
//		// driver.close();
	}

}
