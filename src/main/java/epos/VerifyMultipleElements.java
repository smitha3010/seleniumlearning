package epos;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.github.bonigarcia.wdm.WebDriverManager;

public class VerifyMultipleElements {

	@Test
	public void Amzi(){
	
		
		WebDriverManager.chromedriver().setup();
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://www.amazon.in/");
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		driver.findElementByXPath("//input[@id='twotabsearchtextbox']").sendKeys("Watch");
		driver.findElementByXPath("//input[@id='nav-search-submit-button']").click();
		
		List<WebElement> li=driver.findElementsByXPath("//span[@class='a-size-base-plus a-color-base a-text-normal']");
		
		int size=li.size();
		
		System.out.println(size);
		
		String sh = null;
		
		for (int i = 0; i < size; i++) {
			
			 sh=li.get(i).getText();
			
			System.out.println(sh);
			
			Assert.assertEquals(sh, sh);
			
		}
		
		
		
		
	
	}

}
