package epos;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebElement;


public class WebTable {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://leafground.com/pages/table.html");
		
	 List<WebElement> allRows = driver.findElementsByXPath("//table/tbody/tr");
	 
	 int rowCount=allRows.size();
	 
	 for (int i = 1; i <=rowCount; i++) {
		 
		 List<WebElement> allData = driver.findElementsByXPath("//table/tbody/tr["+i+"]/td");
		int colCount = allData.size();
		 
		 for (int j = 1; j <=colCount; j++) {
			 
String ele=driver.findElementByXPath("//table/tbody/tr["+i+"]/td["+j+"]").getText();
			
			System.out.println(ele);
			
				if(ele.equals("Gopi")) {
					System.out.println("Row number is: "+i);
					System.out.println("Row col number is: "+j);
				
			}
			 
			
		}
		
	}
		
		

	}

}
