package epos;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestLeafGetWindowReuseableMethod {

	public static ChromeDriver driver;

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();

		driver = new ChromeDriver();

		driver.get("http://leafground.com/pages/Window.html");

		driver.manage().window().maximize();

		String parentWin = driver.getWindowHandle();

		System.out.println(parentWin);

		// System.out.println(driver.getTitle());

		driver.findElementByXPath("//button[text()='Open Home Page']").click();

		Set<String> handles = driver.getWindowHandles();

		List<String> hList = new ArrayList<String>(handles);

//		

		if (switchToWindow("TestLeaf - Selenium Playground", hList)) {
			System.out.println(driver.getCurrentUrl() + " " + driver.getTitle());
		}
		Thread.sleep(4000);

		closeAllExceptParent(parentWin, hList);

		switchToParentWindow(parentWin);

		System.out.println(driver.getCurrentUrl() + " " + driver.getTitle());

		String parentTitle = driver.getTitle();

		Assert.assertEquals("TestLeaf - Interact with Windows", parentTitle);
		closeParent(parentWin, hList);

	}

	public static void switchToParentWindow(String parentWin) {
		driver.switchTo().window(parentWin);

	}

	public static void closeAllExceptParent(String parentWin, List<String> hlist) {

		for (String e : hlist) {
			if (!e.equals(parentWin)) {
				driver.switchTo().window(e).close();
			}
		}

	}

	public static void closeParent(String parentWin, List<String> hlist) {

		for (String e1 : hlist) {
			{
				if (e1.equals(parentWin)) {
					driver.switchTo().window(e1).close();
				}

			}
		}

	}

	public static boolean switchToWindow(String winTitle, List<String> hList) {

		for (String e : hList) {

			String title = driver.switchTo().window(e).getTitle();
			if (title.equals(winTitle)) {
				System.out.println("switched to right window");
				return true;
			}
		}
		return false;

	}

}
