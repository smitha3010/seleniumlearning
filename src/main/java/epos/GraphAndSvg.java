package epos;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class GraphAndSvg {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("https://www.google.com/search?q=covid+cases+graph&sxsrf=AOaemvIWYA0IwDfM_VTU0cFpGR8-TmqQ_A%3A1631879771643&ei=W4JEYcG-JofN1sQPqdqbiAo&oq=covid+cases+graph&gs_lcp=Cgdnd3Mtd2l6EAMyCggAEIAEEIcCEBQyCwgAEIAEELEDEMkDMgUIABCSAzIFCAAQkgMyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQ6BwgAEEcQsAM6BAgjECc6CAgAEIAEELEDOg0IABCABBCHAhCxAxAUOgcIABCxAxBDOgoIABCxAxDJAxBDOggIABCxAxCRAjoFCAAQkQI6EAgAEIAEEIcCELEDEMkDEBRKBAhBGABQ7zhYt1Ngl1ZoAHADeACAAZ8BiAGyDpIBBDAuMTOYAQCgAQHIAQjAAQE&sclient=gws-wiz&ved=0ahUKEwiB2Orw-YXzAhWHppUCHSntBqEQ4dUDCA8&uact=5");
		
		List<WebElement> graphlist = driver.findElements(By.xpath("(//*[local-name()='svg' and @class ='uch-psvg'])[1]//*[name()='rect']"));
		Actions act=new Actions(driver);
		
		for (WebElement e : graphlist) {
			act.moveToElement(e).perform();
			String text = driver.findElement(By.xpath("//*[@class='ExnoTd']")).getText();
			System.out.println(text);
				
		}
	}

}
