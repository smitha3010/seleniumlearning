package epos;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AddToCart {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		String[] itemsneeded = { "Brocolli", "Raspberry", "Strawberry","Pomegranate"};
		additems(driver, itemsneeded);

	}

	public static void additems(ChromeDriver driver, String[] itemsneeded) {

		int j = 0;
		List<WebElement> products = driver.findElementsByXPath("//*[@class='product-name']");

		for (int i = 0; i < products.size(); i++) {

			String[] items = products.get(i).getText().split("-");
			String formateditems = items[0].trim();

			List<String> lulu = Arrays.asList(itemsneeded);

			if (lulu.contains(formateditems)) {
				driver.findElementsByXPath("//*[@class='product-action']/button").get(i).click();

				if (j == itemsneeded.length) {
					break;
				}
			}

		}

	}

}
