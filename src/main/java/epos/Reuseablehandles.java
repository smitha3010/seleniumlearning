package epos;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Reuseablehandles {

	public static ChromeDriver driver;

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.get("http://leafground.com/pages/Window.html");

		String parentWin = driver.getWindowHandle();
		System.out.println(parentWin);
		driver.findElementByXPath("//button[text()='Open Home Page']").click();

		Set<String> handles = driver.getWindowHandles();

		List<String> hList = new ArrayList<String>(handles);
		if (switchToRightWindow("TestLeaf - Selenium Playground", hList)) {
			System.out.println(driver.getCurrentUrl() + " " + driver.getTitle());
		}
		Thread.sleep(5000);
		closeAllWinExceptParent(parentWin, hList);
		switchToParentWindow(parentWin);

		System.out.println(driver.getCurrentUrl() + " " + driver.getTitle());

		String parentTitle = driver.getTitle();

		Assert.assertEquals("TestLeaf - Interact with Windows", parentTitle);
		closeParentWin(parentWin, hList);

	}

	public static void closeParentWin(String parentWin, List<String> hList) {

		for (String e1 : hList) {
			if (e1.equals(parentWin)) {
				driver.switchTo().window(e1).close();
			}

		}

	}

	public static void closeAllWinExceptParent(String parentWin, List<String> hList) {

		for (String e : hList) {

			if (!e.equals(parentWin)) {
				driver.switchTo().window(e).close();
			}

		}

	}

	public static void switchToParentWindow(String parentWin) {
		driver.switchTo().window(parentWin);

	}

	public static boolean switchToRightWindow(String winTitle, List<String> hList) {

		for (String e : hList) {
			String title = driver.switchTo().window(e).getTitle();

			if (title.equals(winTitle)) {
				System.out.println("Switched to Right window");
				return true;

			}
		}
		return false;

	}
	
	

}
