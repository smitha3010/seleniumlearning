package epos;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebtablePractise {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		
		driver.get("https://www.w3schools.com/html/html_tables.asp");
		
		int rowCount = driver.findElementsByXPath("//table[@id='customers']//tr").size();
		
		for (int i = 1; i <=rowCount; i++) {
			int colCount = driver.findElementsByXPath("//table[@id='customers']//tr["+i+"]/td").size();
			
			for (int j = 1; j <= colCount ; j++) {
				
			String table= driver.findElementByXPath("//table[@id=\"customers\"]/tbody/tr["+i+"]/td["+j+"]").getText();
			System.out.println(table);
			if(table.equals("Italy")) {
				System.out.println("Row number "+i);
				System.out.println("Col count" +j);
			}
			
			
			
				
			}
			
		}
		
		

	}

}
