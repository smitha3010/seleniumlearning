package epos;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EposAAIThirdPartPolicy {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws InterruptedException, AWTException {

		Robot robot = null;
		WebDriverManager.chromedriver().setup();
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://eposqa.candelalabs.io/");
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.findElementByXPath("//span[text()='Continue to login']").click();
		driver.findElementByXPath("//input[@id='username']").sendKeys("t012@test.com.uat");
		driver.findElementByXPath("//input[@id='password']").sendKeys("Candela@012");
		driver.findElementByXPath("//input[@id='Login']").click();
//		driver.findElementByXPath("//input[@id='login-username']").sendKeys("t005");
//		driver.findElementByXPath("//input[@id='login-password']").sendKeys("clDemo$2020_");
//		driver.findElementByXPath("//button[@id='login-submit']").click();


		Thread.sleep(3000);

		System.out.println("The title is : " + driver.getTitle());

		if (driver.getTitle().contains("AXAAFFIN - FES")) {
			System.out.println("Login sucessfull");
		} else {
			System.out.println("Login Unsucessfull");
		}

		Thread.sleep(3000);

		WebElement ele = driver.findElement(By.xpath("//*[@id=\"btn-new-cff\"]"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click()", ele);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.findElementById("cust_title_button").click();
		driver.findElementById("cust_title_dropbox_item0").click();

		driver.findElementByXPath("//input[@id='cust_fname_input']").sendKeys("Smitha AAI Automation");
		driver.findElementByXPath("//input[@id='cust_lname_input']").sendKeys("practise");

		driver.findElementByXPath("//span[text()='Next']").click();

		Thread.sleep(2000);

		driver.findElementByXPath("//*[@id=\"inputDropDown_conversational-newic-id-no\"]/input").click();

		driver.findElementByXPath("//*[@id=\"inputDropDown_conversational-newic-id-no\"]/input")
				.sendKeys("961111111111");
		driver.findElementByXPath("//*[@id=\"inputDropDown_conversational-newic-id-no\"]/input").sendKeys(Keys.UP);

		Thread.sleep(2000);
		driver.findElementByXPath("//span[text()='Next']").click();

		driver.findElementByXPath("//*[@id=\"cust_smokingStatus_item_1\"]/img").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//span[text()='Next']").click();

		driver.findElementByXPath("//div[text()='Married']").click();
		driver.findElementByXPath("//button[@id=\"cust_occupation_button\"]").click();
		driver.findElementByXPath("//li[@id=\"cust_occupation_dropbox_item4\"]").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//button[@id='spouse_title_button']").click();
		driver.findElementByXPath("//li[text()='Mrs']").click();
		driver.findElementByXPath("//input[@id='spouse_fname_input']").click();
		driver.findElementByXPath("//input[@id='spouse_fname_input']").sendKeys("Spouse AAI test");
		driver.findElementByXPath("//input[@id='spouse_lname_input']").click();
		driver.findElementByXPath("//input[@id='spouse_lname_input']").sendKeys("Last name");
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//*[@id=\"inputDropDown_undefined\"]/input").click();
		driver.findElementByXPath("//*[@id=\"inputDropDown_undefined\"]/input").sendKeys("981111111111");
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(3000);
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/span[2]/img[1]")
				.click();
		driver.findElementByXPath("//button[@id='spouse_occupation_button']").click();
		driver.findElementByXPath("//li[text()='Accountant']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//input[@id='conversational-customer-email-id_input']")
				.sendKeys("smitha.m@candelalabs.io");
		driver.findElementByXPath("//input[@id=\"mobile-number_input\"]").sendKeys("3333333333");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath(
				"//*[@id=\"app-container\"]/div/div/div[3]/div/div[1]/div[1]/div[1]/div/div[1]/div[3]/div[2]/div/span/span[1]/span/input")
				.click();
		driver.findElementByXPath("//button[@id='1_button']").click();
		// li[@id='1_dropbox_item1']
		driver.findElementByXPath("//li[@id='1_dropbox_item1']").click();
		driver.findElementByXPath("//input[@id='2_input']").click();
		driver.findElementByXPath("//input[@id='2_input']").sendKeys("111,111");
		driver.findElementByXPath("//input[@id='3_input']").click();
		driver.findElementByXPath("//input[@id='3_input']").sendKeys("11");
		driver.findElementByXPath("//input[@id='4_input']").click();
		driver.findElementByXPath("//input[@id='4_input']").sendKeys("11111");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//input[@id='FINANCIAL_TEXT_INPUT_1_input']").click();
		driver.findElementByXPath("//input[@id='FINANCIAL_TEXT_INPUT_1_input']").sendKeys("20000");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(2000);
		//driver.findElementByXPath("//span[text()='Next']").sendKeys(Keys.F5);
//		WebDriverWait wait = new WebDriverWait(driver, 20);
//		WebElement element = wait.until(
//		ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.title")));
		String url=driver.getCurrentUrl();
		System.out.println(url);
		//driver.navigate().to(url);
		
		driver.navigate().refresh();
		Thread.sleep(5000);
	//	Thread.sleep(5000);
		//driver.navigate().refresh();
		
		//Thread.sleep(5000);
		driver.findElementByXPath("//button[@id='details-dropdown_button']").click();

		driver.findElementByXPath("//*[text()='AXA Aspire Invest']").click();
		driver.findElementByXPath("//input[@id='1']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();

		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//input[@id='MONTHLY_INCOME_input']").click();
		driver.findElementByXPath("//input[@id='MONTHLY_INCOME_input']").sendKeys("11111");
		driver.findElementByXPath("//input[@id='email-id_input']").click();
		driver.findElementByXPath("//input[@id='email-id_input']").sendKeys("smitha.m@candelalabs.io");
		driver.findElementByXPath("//input[@id='mobile-number_input']").click();
		driver.findElementByXPath("//input[@id='mobile-number_input']").sendKeys("333333333");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("(//input[@id='inputField__1'])[1]").click();
		driver.findElementByXPath("(//input[@id='inputField__1'])[1]").sendKeys("55555");
		driver.findElementByXPath("//div[text()='Funds selection']").click();
		driver.findElementByXPath("(//input[@type='percentage'])[2]").sendKeys("100");
		driver.findElementByXPath("//span[contains(text(),'Generate Illustration')]").click();
		// driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(15000);
		if (driver.findElementByXPath("//span[contains(text(),'roposal Specially Designed for You')]").isDisplayed()) {
			System.out.println("SI pdf Generated");
		} else {
			System.out.println("SI pdf not generated");
		}

		driver.findElementByXPath("//body/div[2]/div[3]/div[1]/*[1]").click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//input[@id='isSelected-0']").click();
		driver.findElementByXPath("//span[text()='Next']").click();

		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[9]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("My city");
		driver.findElementByXPath("//input[@id='residence_address_input']").sendKeys("No 19");
		driver.findElementByXPath("//input[@id='residence_addressline2New_input']").sendKeys("1 street");
		driver.findElementByXPath("//input[@id='residence_addressline3New_input']").sendKeys("city");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[5]/div[1]/div[1]/button[1]")
				.click();
		driver.findElementByXPath("//li[@id='state_dropbox_item0']").click();
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[8]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("45678");
		driver.findElementByXPath("//span[contains(text(),'Yes')]").click();
		driver.findElementByXPath("//div[contains(text(),'Occupation Details')]").click();
		driver.findElementByXPath("//*[@id=\"cust_gender_item_0\"]").click();
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("Candela");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[5]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("address line 1");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[6]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("address line 2");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[7]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("address line 3");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[10]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("Automobile");
		driver.findElementByXPath("//input[@id='inputField__1']").click();
		driver.findElementByXPath("//input[@id='inputField__1']").sendKeys("22222");
		driver.findElementByXPath("//label[contains(text(),'Spouse AAI test Last name')]").click();
		driver.findElementByXPath("//div[contains(text(),'General Details')]").click();
		driver.findElementByXPath("(//input[@id='cityofbirth_input'])[1]").click();
		driver.findElementByXPath("(//input[@id='cityofbirth_input'])[1]").sendKeys("city");
		driver.findElementByXPath("(//span[@id='cust_gender_item_0'])[2]").click();
		driver.findElementByXPath("//div[contains(text(),'Occupation Details')]").click();
		driver.findElementByXPath("//span[@id='cust_gender_item_0']").click();
		driver.findElementByXPath("(//input[@id='postal_code_input'])[1]").click();
		driver.findElementByXPath("(//input[@id='postal_code_input'])[1]").sendKeys("Jin yoon");
		driver.findElementByXPath("(//input[@id='postal_code_input'])[3]").sendKeys("Address line 1");
		driver.findElementByXPath("(//input[@id='postal_code_input'])[4]").sendKeys("Address line 2");
		driver.findElementByXPath("(//input[@id='postal_code_input'])[5]").sendKeys("Address line 3");
		driver.findElementByXPath("(//input[@id='postal_code_input'])[7]").sendKeys("Automobile");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//label[contains(text(),'Spouse AAI test Last name')]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[1]").click();
		driver.findElementByXPath("//button[@id='detailedQuestion1List-dropdown_button']").click();
		driver.findElementByXPath("//li[@id='detailedQuestion1List-dropdown_dropbox_item0']").click();
		driver.findElementByXPath("(//textarea[@id='textArea_textAreaContainer'])[1]").click();
		driver.findElementByXPath("(//textarea[@id='textArea_textAreaContainer'])[1]").sendKeys("Cancer details");
		driver.findElementByXPath("(//input[@type='checkbox'])[2]").click();
		driver.findElementByXPath("(//textarea[@id='textArea_textAreaContainer'])[2]").sendKeys("cold");
		driver.findElementByXPath("//div[contains(text(),'BMI Details')]").click();
		driver.findElementByXPath("//input[@id='height_input']").sendKeys("155");
		driver.findElementByXPath("//input[@id='Weight_input']").sendKeys("55");
		driver.findElementByXPath("//button[@id='gain_loss_button']").click();
		driver.findElementByXPath("//li[@id='gain_loss_dropbox_item2']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//label[contains(text(),'Spouse AAI test Last name')]").click();
		driver.findElementByXPath("//div[contains(text(),'Deferred/Declined Application')]").click();
		driver.findElementByXPath("(//span[@id='buttonSpan_0'])[1]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//span[contains(text(),'Investment/Asset')]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[contains(text(),'Beneficial Owner')]").click();
		driver.findElementByXPath("//div[contains(text(),'Payor Information')]").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//label[contains(text(),'Spouse AAI test Last name')]").click();
		driver.findElementByXPath("//div[contains(text(),'FATCA Questionnaire')]").click();
		driver.findElementByXPath("//div[contains(text(),'COMMON REPORTING STANDARD (CRS)')]").click();
		driver.findElementByXPath("//div[contains(text(),'Marketing Data Consent')]").click();
		driver.findElementByXPath("//div[contains(text(),'Data Protection Statement')]").click();
		driver.findElementByXPath("//div[contains(text(),'PEP Declaration')]").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//button[@id='details-dropdown_button']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//li[@id='details-dropdown_dropbox_item0']").click();
		driver.findElementByXPath("//input[@id='bank_account_number_input']").click();
		driver.findElementByXPath("//input[@id='bank_account_number_input']").sendKeys("111");
		driver.findElementByXPath("//input[@id='confirm_bank_account_number_input']").click();
		driver.findElementByXPath("//input[@id='confirm_bank_account_number_input']").sendKeys("111");
		driver.findElementByXPath("//span[contains(text(),'Individual')]").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(3000);
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("Witness name");
		driver.findElementByXPath("//div[contains(text(),'Birth Certificate')]").click();
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[3]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("birth certificate");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[4]/div[1]/span[1]/span[1]/input[1]")
				.click();
		Thread.sleep(3000);
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[4]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("Malaysia");
		Thread.sleep(3000);

		driver.findElementByXPath("//p[contains(text(),'Capture below signatures')]").click();
//		
		driver.findElementByXPath(
				"//body[1]/section[1]/div[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[1]/button[1]").click();
		Thread.sleep(10000);
		if (driver.findElementByXPath("//span[contains(text(),'roposal Specially Designed for You')]").isDisplayed()) {
			System.out.println("SI pdf Generated in signature page");
		} else {
			System.out.println("SI pdf not generated in signature page ");

		}
		driver.findElementByXPath("//body/div[2]/div[3]/div[1]/*[1]").click();

		WebElement canvasElement1 = driver.findElement(By.xpath("(//canvas[@class='sigCanvas'])[1]"));
		Actions builder1 = new Actions(driver);
		builder1.clickAndHold(canvasElement1).moveByOffset(20,- 50).
		moveByOffset(50,50).
		moveByOffset(80,-50).
		moveByOffset(100,50).
		release().perform();
		builder1.clickAndHold(canvasElement1).moveByOffset(15,- 40).
		moveByOffset(40,40).
		moveByOffset(70,-40).
		moveByOffset(90,40).
		release().perform();
		Thread.sleep(3000);
		driver.findElementByXPath("(//p[@class='enabled-p'])[1]").click();
		//driver.findElementByXPath("(//p[text()='Save'])[1]").click();
//		WebElement ele4 = driver.findElement(By.xpath("(//p[text()='Save'])[1]"));
//		JavascriptExecutor jse4 = (JavascriptExecutor) driver;
//		jse4.executeScript("arguments[0].click()", ele4);
	//	driver.findElementByXPath("//p[contains(text(),'Capture below signatures')]").click();
		Thread.sleep(5000);
		WebElement canvasElement2 = driver.findElement(By.xpath("(//canvas[@class='sigCanvas'])[2]"));
		Actions builder2 = new Actions(driver);
		builder2.clickAndHold(canvasElement2).moveByOffset(20,- 50).
		moveByOffset(50,50).
		moveByOffset(80,-50).
		moveByOffset(100,50).
		release().perform();
		builder2.clickAndHold(canvasElement2).moveByOffset(15,- 40).
		moveByOffset(40,40).
		moveByOffset(70,-40).
		moveByOffset(90,40).
		release().perform();
		Thread.sleep(3000);
		driver.findElementByXPath("(//p[@class='enabled-p'])[2]").click();
		//driver.findElementByXPath("(//p[text()='Save'])[2]").click();
//		WebElement ele3 = driver.findElement(By.xpath("(//p[text()='Save'])[2]"));
//		JavascriptExecutor jse3 = (JavascriptExecutor) driver;
//		jse3.executeScript("arguments[0].click()", ele3);
		//driver.findElementByXPath("//p[contains(text(),'Capture below signatures')]").click();
		
		Thread.sleep(6000);
		WebElement canvasElement3 = driver.findElementByXPath("(//canvas[@class='sigCanvas'])[3]");
		Actions builder3 = new Actions(driver);
		builder3.clickAndHold(canvasElement3).moveByOffset(20,- 50).
		moveByOffset(50,50).
		moveByOffset(80,-50).
		moveByOffset(100,50).
		release().perform();
		builder3.clickAndHold(canvasElement3).moveByOffset(15,- 40).
		moveByOffset(40,40).
		moveByOffset(70,-40).
		moveByOffset(90,40).
		release().perform();
		Thread.sleep(2000);
		driver.findElementByXPath("(//p[@class='enabled-p'])[3]").click();
//		WebElement ele2 = driver.findElement(By.xpath("(//p[text()='Save'])[3]"));
//		JavascriptExecutor jse2 = (JavascriptExecutor) driver;
//		jse2.executeScript("arguments[0].click()", ele2);
		
	//	driver.findElementByXPath("(//p[text()='Save'])[3]").click();
		//driver.findElementByXPath("//p[contains(text(),'Capture below signatures')]").click();
		Thread.sleep(5000);
		// browse and open the csv file from the PC location
					// Use the robot class
					// Copy the CSV file path
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(2000);
		
		driver.findElementByXPath("//div[@id='ID-front-doc_upload-icon']").click();
		Thread.sleep(6000);
		
		
					StringSelection strPath1 = new StringSelection("file:///C:/Users/Admin/Desktop/smitha.webp");
					Clipboard clipBoard1 = Toolkit.getDefaultToolkit().getSystemClipboard();
					clipBoard1.setContents(strPath1, null);
//
					// PRess Ctrl + V
					robot = new Robot();
					robot.keyPress(KeyEvent.VK_CONTROL);
					robot.keyPress(KeyEvent.VK_V);
					Thread.sleep(2000);
					robot.keyRelease(KeyEvent.VK_CONTROL);

					Thread.sleep(3000);

					robot.keyPress(KeyEvent.VK_ENTER);
					Thread.sleep(2000);
					robot.keyRelease(KeyEvent.VK_ENTER);
					
//					
//////					
					driver.findElementByXPath("//div[@id='ID-back-doc_upload-icon']").click();
					Thread.sleep(6000);
					
					// browse and open the pdf file from the PC location
								// Use the robot class
								// Copy the pdf file path
								StringSelection strPath = new StringSelection("D:\\OnlineClasses\\CoreJava\\Sample.pdf");
								Clipboard clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
								clipBoard.setContents(strPath, null);

								// PRess Ctrl + V
								robot = new Robot();
								robot.keyPress(KeyEvent.VK_CONTROL);
								robot.keyPress(KeyEvent.VK_V);
								Thread.sleep(2000);
								robot.keyRelease(KeyEvent.VK_CONTROL);

								Thread.sleep(3000);

								robot.keyPress(KeyEvent.VK_ENTER);
								Thread.sleep(2000);
								robot.keyRelease(KeyEvent.VK_ENTER);
								
								Thread.sleep(6000);
								
								driver.findElementByXPath("//label[contains(text(),'Spouse AAI test Last name')]").click();
								driver.findElementByXPath("//div[@id='ID-front-doc_upload-icon']").click();
								Thread.sleep(4000);
////								
								// browse and open the csv file from the PC location
											// Use the robot class
											// Copy the CSV file path
											StringSelection strPath3 = new StringSelection("D:\\OnlineClasses\\CoreJava\\Sample.pdf");
											Clipboard clipBoard3 = Toolkit.getDefaultToolkit().getSystemClipboard();
											clipBoard1.setContents(strPath1, null);
						//
											// PRess Ctrl + V
											robot = new Robot();
											robot.keyPress(KeyEvent.VK_CONTROL);
											robot.keyPress(KeyEvent.VK_V);
											Thread.sleep(3000);
											robot.keyRelease(KeyEvent.VK_CONTROL);

											Thread.sleep(3000);

											robot.keyPress(KeyEvent.VK_ENTER);
											Thread.sleep(3000);
											robot.keyRelease(KeyEvent.VK_ENTER);
											
//											
//////											
											driver.findElementByXPath("//div[@id='ID-back-doc_upload-icon']").click();
											Thread.sleep(6000);
											
											// browse and open the csv file from the PC location
														// Use the robot class
														// Copy the CSV file path
														StringSelection strPath2 = new StringSelection("D:\\OnlineClasses\\CoreJava\\Sample.pdf");
														Clipboard clipBoard2 = Toolkit.getDefaultToolkit().getSystemClipboard();
														clipBoard1.setContents(strPath, null);

														// PRess Ctrl + V
														robot = new Robot();
														robot.keyPress(KeyEvent.VK_CONTROL);
														robot.keyPress(KeyEvent.VK_V);
														Thread.sleep(3000);
														robot.keyRelease(KeyEvent.VK_CONTROL);

														Thread.sleep(2000);

														robot.keyPress(KeyEvent.VK_ENTER);
														Thread.sleep(3000);
														robot.keyRelease(KeyEvent.VK_ENTER);
														
														driver.findElementByXPath("//span[text()='Next']").click();
														Thread.sleep(6000);
														System.out.println(driver.findElementByXPath("//input[@type='checkbox']").isSelected());
														Thread.sleep(5000);
														
														WebElement ele1 = driver.findElement(By.xpath("//input[@type='checkbox']"));
														JavascriptExecutor executor = (JavascriptExecutor)driver;
														executor.executeScript("arguments[0].click();", ele1);
													
														driver.findElementByXPath("//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/span[2]").click();
														driver.findElementByXPath("//button[text()='Pay Later']").click();
														driver.findElementByXPath("//*[@id=\"body\"]/div[2]/div[3]/div[4]/button[1]").click();
														Thread.sleep(2000);
														driver.findElementByXPath("//button[contains(text(),'Submit')]").click();
														Thread.sleep(30000);
														driver.findElementByXPath("//span[contains(text(),'Go to Home')]").click();
														
		
		
		

	}

}
