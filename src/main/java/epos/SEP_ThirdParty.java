package epos;

import java.awt.Robot;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SEP_ThirdParty {

	public static void main(String[] args) throws InterruptedException {
		Robot robot = null;
		WebDriverManager.chromedriver().setup();
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://eposqa.candelalabs.io/");
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.findElementByXPath("//span[text()='Continue to login']").click();
		driver.findElementByXPath("//input[@id='username']").sendKeys("t025@test.com.uat");
		driver.findElementByXPath("//input[@id='password']").sendKeys("test@025");
		driver.findElementByXPath("//input[@id='Login']").click();

		Thread.sleep(3000);

		System.out.println("The title is : " + driver.getTitle());

		if (driver.getTitle().contains("AXAAFFIN - FES")) {
			System.out.println("Login sucessfull");
		} else {
			System.out.println("Login Unsucessfull");
		}

		Thread.sleep(3000);

		WebElement ele = driver.findElement(By.xpath("//*[@id=\"btn-new-cff\"]"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click()", ele);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.findElementById("cust_title_button").click();
		driver.findElementById("cust_title_dropbox_item0").click();

		driver.findElementByXPath("//input[@id='cust_fname_input']").sendKeys("Smitha AAI Automation");
		driver.findElementByXPath("//input[@id='cust_lname_input']").sendKeys("practise");

		driver.findElementByXPath("//span[text()='Next']").click();

		Thread.sleep(2000);

		driver.findElementByXPath("//*[@id=\"inputDropDown_conversational-newic-id-no\"]/input").click();

		driver.findElementByXPath("//*[@id=\"inputDropDown_conversational-newic-id-no\"]/input")
				.sendKeys("961111111111");
		driver.findElementByXPath("//*[@id=\"inputDropDown_conversational-newic-id-no\"]/input").sendKeys(Keys.UP);

		Thread.sleep(2000);
		driver.findElementByXPath("//span[text()='Next']").click();

		driver.findElementByXPath("//*[@id=\"cust_smokingStatus_item_1\"]/img").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//span[text()='Next']").click();

		driver.findElementByXPath("//div[text()='Married']").click();
		driver.findElementByXPath("//button[@id=\"cust_occupation_button\"]").click();
		driver.findElementByXPath("//li[@id=\"cust_occupation_dropbox_item4\"]").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//button[@id='spouse_title_button']").click();
		driver.findElementByXPath("//li[text()='Mrs']").click();
		driver.findElementByXPath("//input[@id='spouse_fname_input']").click();
		driver.findElementByXPath("//input[@id='spouse_fname_input']").sendKeys("Spouse AAI test");
		driver.findElementByXPath("//input[@id='spouse_lname_input']").click();
		driver.findElementByXPath("//input[@id='spouse_lname_input']").sendKeys("Last name");
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//*[@id=\"inputDropDown_undefined\"]/input").click();
		driver.findElementByXPath("//*[@id=\"inputDropDown_undefined\"]/input").sendKeys("981111111111");
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(3000);
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/span[2]/img[1]")
				.click();
		driver.findElementByXPath("//button[@id='spouse_occupation_button']").click();
		driver.findElementByXPath("//li[text()='Accountant']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//input[@id='conversational-customer-email-id_input']")
				.sendKeys("smitha.m@candelalabs.io");
		driver.findElementByXPath("//input[@id=\"mobile-number_input\"]").sendKeys("3333333333");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath(
				"//*[@id=\"app-container\"]/div/div/div[3]/div/div[1]/div[1]/div[1]/div/div[1]/div[3]/div[2]/div/span/span[1]/span/input")
				.click();
		driver.findElementByXPath("//button[@id='1_button']").click();
		// li[@id='1_dropbox_item1']
		driver.findElementByXPath("//li[@id='1_dropbox_item1']").click();
		driver.findElementByXPath("//input[@id='2_input']").click();
		driver.findElementByXPath("//input[@id='2_input']").sendKeys("111,111");
		driver.findElementByXPath("//input[@id='3_input']").click();
		driver.findElementByXPath("//input[@id='3_input']").sendKeys("11");
		driver.findElementByXPath("//input[@id='4_input']").click();
		driver.findElementByXPath("//input[@id='4_input']").sendKeys("11111");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//input[@id='FINANCIAL_TEXT_INPUT_1_input']").click();
		driver.findElementByXPath("//input[@id='FINANCIAL_TEXT_INPUT_1_input']").sendKeys("20000");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//button[@id='details-dropdown_button']").click();
		driver.findElementByXPath("//li[text()='AFFIN Secure Enhance Plus']").click();
		driver.findElementByXPath("//input[@id='1']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(3000);
	}

}
