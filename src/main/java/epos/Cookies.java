package epos;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Cookies {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.ebay.com/");
		Set<Cookie> cookies = driver.manage().getCookies();
		
		for (Cookie eachelement : cookies) {
			
			System.out.println(eachelement);
			
		}
		
//		Iterator<Cookie> itr = cookies.iterator();
//		
//		while(itr.hasNext())
//		{
//			System.out.println(itr.next());
//		}
		driver.quit();
		

	}

}
