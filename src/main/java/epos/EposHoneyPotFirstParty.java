package epos;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EposHoneyPotFirstParty {

	public static void main(String[] args) throws InterruptedException, AWTException {
		Robot robot = null;
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		
		// driver.get("https://eposdemo.candelalabs.io/");
		driver.navigate().refresh();
		driver.navigate().to("https://eposdemo.candelalabs.io/");
		Thread.sleep(5000);

		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.navigate().refresh();

//		//driver.findElementByXPath("//span[text()='Continue to login']").click();
		driver.findElementByXPath("//*[@id='login-username']").sendKeys("john.doe");
		driver.findElementByXPath("//*[@id='login-password']").sendKeys("clDemo$2020_");
		driver.findElementByXPath("//*[@id='login-submit']").click();
//
		Thread.sleep(5000);
//
////		System.out.println("The title is : " + driver.getTitle());
////
////		if (driver.getTitle().contains("AXAAFFIN - FES")) {
////			System.out.println("Login sucessfull");
////		} else {
////			System.out.println("Login Unsucessfull");
////		}
////        
//		Thread.sleep(3000);
//
		WebElement ele = driver.findElement(By.xpath("//*[@id=\"btn-new-cff\"]"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click()", ele);
//
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//
		driver.findElementById("cust_title_button").click();
		driver.findElementById("cust_title_dropbox_item0").click();
//
		driver.findElementByXPath("//input[@id='cust_fname_input']").sendKeys(" HoneyPot Automation");
		driver.findElementByXPath("//input[@id='cust_lname_input']").sendKeys("practise");
//
		driver.findElementByXPath("//span[text()='Next']").click();
//
		Thread.sleep(2000);
//
		driver.findElementByXPath("//*[@id=\"inputDropDown_conversational-newic-id-no\"]/input").click();
//
		driver.findElementByXPath("//*[@id=\"inputDropDown_conversational-newic-id-no\"]/input")
				.sendKeys("961111111111");
		driver.findElementByXPath("//*[@id=\"inputDropDown_conversational-newic-id-no\"]/input").sendKeys(Keys.UP);
//
		Thread.sleep(2000);
		driver.findElementByXPath("//span[text()='Next']").click();
//
		driver.findElementByXPath("//*[@id=\"cust_smokingStatus_item_1\"]/img").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//span[text()='Next']").click();
//
		driver.findElementByXPath("//div[contains(text(),'Single')]").click();
		driver.findElementByXPath("//button[@id=\"cust_occupation_button\"]").click();
		driver.findElementByXPath("//li[@id=\"cust_occupation_dropbox_item4\"]").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//input[@id='conversational-customer-email-id_input']")
				.sendKeys("smithagowda.m@gmail.com");
		driver.findElementByXPath("//input[@id=\"mobile-number_input\"]").sendKeys("3333333333");
		driver.findElementByXPath("//*[@class='nextLogo']").click();
		driver.findElementByXPath("//*[@class='nextLogo']").click();
		driver.findElementByXPath("//*[@class='nextLogo']").click();
		driver.findElementByXPath(
				"//*[@id=\"app-container\"]/div/div/div[3]/div/div[1]/div[1]/div[1]/div/div[1]/div[3]/div[2]/div/span/span[1]/span/input")
				.click();
		driver.findElementByXPath("//button[@id='1_button']").click();
		driver.findElementByXPath("//li[@id='1_dropbox_item0']").click();
		driver.findElementByXPath("//input[@id='2_input']").click();
		driver.findElementByXPath("//input[@id='2_input']").sendKeys("10000");
		driver.findElementByXPath("//input[@id='3_input']").click();
		driver.findElementByXPath("//input[@id='3_input']").sendKeys("2");
		driver.findElementByXPath("//input[@id='4_input']").click();
		driver.findElementByXPath("//input[@id='4_input']").sendKeys("200");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//input[@id='FINANCIAL_TEXT_INPUT_1_input']").click();
		driver.findElementByXPath("//input[@id='FINANCIAL_TEXT_INPUT_1_input']").sendKeys("2000");
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(20000);
		driver.findElementByXPath("//button[@id='details-dropdown_button']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[text()='HoneyPot']").click();
		driver.findElementByXPath("//input[@type='checkbox']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//span[contains(text(),'Next')]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//span[contains(text(),'Next')]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//span[contains(text(),'Next')]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//input[@id='MONTHLY_INCOME_input']").sendKeys("9999");
		driver.findElementByXPath("//span[contains(text(),'Next')]").click();
		driver.findElementByXPath("(//input[@id='inputField__1'])[2]").sendKeys("5000");
		Thread.sleep(2000);
		driver.findElementByXPath("//span[contains(text(),'Next')]").click();
		driver.findElementByXPath("//span[contains(text(),'Next')]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//input[@type='percentage'])[2]").sendKeys("100");
		Thread.sleep(3000);
//		
//		
		driver.findElementByXPath("//span[contains(text(),'Generate Illustration')]").click();
		// driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Thread.sleep(15000);
		if (driver.findElementByXPath("//span[contains(text(),'roposal Specially Designed for You')]").isDisplayed()) {
			System.out.println("SI pdf Generated");
		} else {
			System.out.println("SI pdf not generated");
		}
//
		driver.findElementByXPath("//body/div[2]/div[3]/div[1]/*[1]").click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//input[@id='isSelected-0']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
//		
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[9]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("My city");
		driver.findElementByXPath("//input[@id='residence_address_input']").sendKeys("No 19");
		driver.findElementByXPath("//input[@id='residence_addressline2New_input']").sendKeys("1 street");
		driver.findElementByXPath("//input[@id='residence_addressline3New_input']").sendKeys("city");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[5]/div[1]/div[1]/button[1]")
				.click();
		driver.findElementByXPath("//li[@id='state_dropbox_item0']").click();
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[4]/div[1]/div[1]/div[8]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("45678");
		driver.findElementByXPath("//span[contains(text(),'Yes')]").click();
		driver.findElementByXPath("//div[contains(text(),'Occupation Details')]").click();
		driver.findElementByXPath("//*[@id=\"cust_gender_item_0\"]").click();
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[3]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("Candela");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[5]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("address line 1");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[6]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("address line 2");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[7]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("address line 3");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[10]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("Automobile");
		driver.findElementByXPath("//span[text()='Next']").click();
//				
		Thread.sleep(3000);
//				
		driver.findElementByXPath("(//input[@type='checkbox'])[3]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[4]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[5]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[6]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[7]").click();
		driver.findElementByXPath("//div[contains(text(),'Family History')]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[1]").click();
		driver.findElementByXPath("//div[contains(text(),'Health Details')]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[1]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[2]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[3]").click();
		Thread.sleep(1500);
//		
		driver.findElementByXPath(
				"//*[@id=\"app-container\"]/div/div/div[3]/div/div/div[2]/div/div[2]/div/div/div/div[4]/div[1]/p/div[2]/span/div/div/span[2]/span[1]/span/input")
				.click();
		driver.findElementByXPath(
				"//*[@id=\"app-container\"]/div/div/div[3]/div/div/div[2]/div/div[2]/div/div/div/div[5]/div[1]/p/div[2]/span/div/div/span[2]/span[1]/span/input")
				.click();
		driver.findElementByXPath(
				"//*[@id=\"app-container\"]/div/div/div[3]/div/div/div[2]/div/div[2]/div/div/div/div[6]/div[1]/p/div[2]/span/div/div/span[2]/span[1]/span/input")
				.click();
//		
//														
		driver.findElementByXPath("//div[contains(text(),'BMI Details')]").click();
		driver.findElementByXPath("//input[@id='height_input']").sendKeys("155");
		driver.findElementByXPath("//input[@id='Weight_input']").sendKeys("55");
		driver.findElementByXPath("//button[@id='gain_loss_button']").click();
		driver.findElementByXPath("//li[@id='gain_loss_dropbox_item2']").click();
//													
		driver.findElementByXPath("//div[contains(text(),'Regular Doctor Details')]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[1]").click();
		driver.findElementByXPath("//div[contains(text(),'Detailed Health Questions')]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[1]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[2]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[3]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[4]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[5]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[6]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[7]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[8]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[9]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[10]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[11]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[12]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[13]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[14]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[15]").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//div[contains(text(),'Insurance History')]").click();
		driver.findElementByXPath("//div[contains(text(),'Deferred/Declined Application')]").click();
		driver.findElementByXPath("(//input[@type='checkbox'])[1]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\"body\"]/div[2]/div[3]/div[4]/button").click();
//		
		driver.findElementByXPath("(//input[@type='checkbox'])[2]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//*[@id=\"body\"]/div[2]/div[3]/div[4]/button").click();
		// driver.findElementByXPath("//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/span[2]/span[1]/span[1]/input[1]").click();
		Thread.sleep(2000);
		driver.findElementByXPath("(//span[@class='txt-label'])[1]").click();
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(6000);
		driver.findElementByXPath("(//span[@id='buttonSpan_1'])[2]").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//div[contains(text(),'Beneficial Owner')]").click();
//		
		driver.findElementByXPath("//div[contains(text(),'Payor Information')]").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(10000);
		driver.findElementByXPath("//div[@class='toggle-btn active']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//div[@class='toggle-btn active']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[@id='details-radio_item_1']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		driver.findElementByXPath("(//div[@class='toggle-btn active'])[1]").click();
		Thread.sleep(1000);
		driver.findElementByXPath(
				"//*[@id=\"app-container\"]/div/div/div[3]/div[2]/div/div/div/div[2]/div[4]/div/div/div/div[2]/div/div/div/div")
				.click();
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//button[@id='details-dropdown_button']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//li[@id='details-dropdown_dropbox_item0']").click();
		driver.findElementByXPath("//input[@id='bank_account_number_input']").click();
		driver.findElementByXPath("//input[@id='bank_account_number_input']").sendKeys("111");
		driver.findElementByXPath("//input[@id='confirm_bank_account_number_input']").click();
		driver.findElementByXPath("//input[@id='confirm_bank_account_number_input']").sendKeys("111");
		driver.findElementByXPath("//span[contains(text(),'Individual')]").click();
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(3000);
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[1]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("Witness name");
		driver.findElementByXPath("//div[contains(text(),'Birth Certificate')]").click();
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[3]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("birth certificate");
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[4]/div[1]/span[1]/span[1]/input[1]")
				.click();
		Thread.sleep(3000);
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[4]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("Malaysia");
		Thread.sleep(3000);
//		
		driver.findElementByXPath("//p[contains(text(),'Capture below signatures')]").click();
////		
		driver.findElementByXPath(
				"//body[1]/section[1]/div[1]/div[1]/div[3]/div[1]/div[3]/div[1]/div[1]/div[1]/button[1]").click();
		Thread.sleep(10000);
		if (driver.findElementByXPath("//span[contains(text(),'roposal Specially Designed for You')]").isDisplayed()) {
			System.out.println("SI pdf Generated in signature page");
		} else {
			System.out.println("SI pdf not generated in signature page ");
		}
//		
		driver.findElementByXPath("//*[@id=\"app-container\"]/div/div/div[3]/div/div[4]/div/div[1]/div/div[1]").click();
//		
		WebElement canvasElement1 = driver.findElement(By.xpath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]"));
		Actions builder1 = new Actions(driver);
		builder1.clickAndHold(canvasElement1).moveByOffset(10, 50).moveByOffset(50, 10).moveByOffset(-10, -50)
				.moveByOffset(-50, -10).release().perform();
		driver.findElementByXPath("//p[contains(text(),'Capture below signatures')]").click();
		Thread.sleep(10000);
		WebElement canvasElement2 = driver.findElement(By.xpath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/canvas[1]"));
		Actions builder2 = new Actions(driver);
		builder2.clickAndHold(canvasElement2).moveByOffset(10, 50).moveByOffset(50, 10).moveByOffset(-10, -50)
				.moveByOffset(-50, -10).release().perform();
		driver.findElementByXPath("//p[contains(text(),'Capture below signatures')]").click();
		Thread.sleep(20000);
		WebElement canvasElement3 = driver.findElement(By.xpath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[2]/div[1]/div[1]/canvas[1]"));
		Actions builder3 = new Actions(driver);
		builder3.clickAndHold(canvasElement3).moveByOffset(10, 50).moveByOffset(50, 10).moveByOffset(-10, -50)
				.moveByOffset(-50, -10).release().perform();
		driver.findElementByXPath("//p[contains(text(),'Capture below signatures')]").click();
		Thread.sleep(2000);
////		
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[4]/div[1]/span[1]/span[1]/input[1]")
				.click();
		Thread.sleep(2000);
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[4]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("Malaysia");
		Thread.sleep(3000);
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[4]/div[1]/span[1]/span[1]/input[1]")
				.clear();
		Thread.sleep(3000);
		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[5]/div[1]/div[1]/div[4]/div[1]/span[1]/span[1]/input[1]")
				.sendKeys("smitha");
		Thread.sleep(2000);
		driver.findElementByXPath("(//div[@class='signature-container'])[3]").click();
		Thread.sleep(2000);
////		
////		
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//body[1]/section[1]/div[1]/div[1]/div[1]/div[1]/div[7]/span[1]").click();
		Thread.sleep(2000);
////		
		driver.findElementByXPath("//div[@id='ID-front-doc_upload-icon']").click();
		Thread.sleep(3000);
//////		
		// browse and open the csv file from the PC location
		// Use the robot class
		// Copy the CSV file path
		StringSelection strPath1 = new StringSelection("D:\\OnlineClasses\\CoreJava\\Sample.pdf");
		Clipboard clipBoard1 = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipBoard1.setContents(strPath1, null);
////
		// PRess Ctrl + V
		robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		Thread.sleep(2000);
		robot.keyRelease(KeyEvent.VK_CONTROL);
//
		Thread.sleep(2000);

		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		robot.keyRelease(KeyEvent.VK_ENTER);
//					
////					
////////					
		driver.findElementByXPath("//div[@id='ID-back-doc_upload-icon']").click();
		Thread.sleep(6000);
//					
		// browse and open the csv file from the PC location
		// Use the robot class
		// Copy the CSV file path
		StringSelection strPath = new StringSelection("D:\\OnlineClasses\\CoreJava\\Sample.pdf");
		Clipboard clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipBoard.setContents(strPath, null);

		// PRess Ctrl + V
		robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		Thread.sleep(2000);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		Thread.sleep(2000);

		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		robot.keyRelease(KeyEvent.VK_ENTER);
//								
//								
////						
//////								
		driver.findElementByXPath("//span[text()='Next']").click();
		Thread.sleep(6000);
		System.out.println(driver.findElementByXPath("//input[@type='checkbox']").isSelected());
		Thread.sleep(5000);

		WebElement ele1 = driver.findElement(By.xpath("//input[@type='checkbox']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele1);

		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/span[2]")
				.click();

		driver.findElementByXPath(
				"//body/section[@id='app-container']/div[1]/div[1]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/span[2]")
				.click();
		// driver.findElementByXPath("//button[text()='Pay Later']").click();
		// driver.findElementByXPath("//*[@id=\"body\"]/div[2]/div[3]/div[4]/button[1]").click();
		// Thread.sleep(2000);
		// driver.findElementByXPath("//button[contains(text(),'Submit')]").click();
		// Thread.sleep(10000);
		// driver.findElementByXPath("//span[contains(text(),'Go to Home')]").click();
////								
//////					
//////		
////		
//		
//		
//		
//		
//		
//		
//		//driver.close();
//		
//

	}

}
