package epos;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ReuseableMethodForGetWindowHnadles {

	public static ChromeDriver driver;

	public static void main(String[] args) throws InterruptedException {

		WebDriverManager.chromedriver().setup();

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.get("https://opensourcedemo-demo.orangehrmlive.com/");

		String parentWindow = driver.getWindowHandle();
		Thread.sleep(4000);

		driver.findElementByXPath("//a[@href='//www.facebook.com/OrangeHRM']//i").click();
//		driver.navigate().back();
//
//		driver.findElementByXPath("//a[@href='//twitter.com/orangehrm']//i").click();
//		driver.navigate().back();
//		driver.findElementByXPath("//a[@href='//plus.google.com/+OrangeHRM/']//i").click();
//		driver.navigate().back();
//		driver.findElementByXPath("//a[@href='//www.linkedin.com/company/orangehrm']//i").click();
//		driver.navigate().back();
//		driver.findElementByXPath("//a[@href='//www.youtube.com/user/orangehrm']//i").click();
//		driver.navigate().back();

		Set<String> handles = driver.getWindowHandles();
		List<String> hlist = new ArrayList<String>(handles);
		if (switchToRightWindow("Facebook", hlist)) {
			System.out.println(driver.getCurrentUrl() + ":" + driver.getTitle());
		}

		switchToParentWindow();
		System.out.println(driver.getCurrentUrl() + ":" + driver.getTitle());
		closeParentWindow();

	}

	public static void switchToParentWindow() {
		driver.navigate().back();
	}

	public static void closeParentWindow() {
		driver.close();

	}

	public static void closeAllExceptParent(String parentwindow, List<String> hlist) {

		for (String e : hlist) {
			if (!e.equals(parentwindow)) {
				driver.switchTo().window(e).close();
			}
		}

	}

	public static boolean switchToRightWindow(String windowTitle, List<String> hlist) {

		for (String e : hlist) {

			String title = driver.switchTo().window(e).getTitle();
			if (title.contains(windowTitle)) {
				System.out.println("found the right window");
				return true;
			}

		}
		return false;

	}

}
