package epos;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SmithareuseableGetWindow {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		
		ChromeDriver driver= new ChromeDriver();
		
		driver.get("http://leafground.com/pages/Window.html");
		
		String parentWin=driver.getWindowHandle();
		System.out.println(parentWin);
		driver.findElementByXPath("//button[text()='Open Home Page']").click();
		
		Set<String> allwindows=driver.getWindowHandles();
		
		List<String> hList=new ArrayList<String>(allwindows);
		String secWin=null;
		for (String e : hList) {
			
			if(!e.equals(parentWin))
			{
				 secWin=hList.get(1);
			}
			
			
		}
		
		System.out.println(driver.switchTo().window(secWin).getTitle());
	
		
		
		
		
		
		
		

	}

}
