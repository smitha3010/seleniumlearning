package epos;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestAlertandFrame {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();
		
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_prompt");
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//		
//		 WebElement framexpath = driver.findElementByXPath("//button[contains(text(),'Try it')]");
//		Thread.sleep(2000);
//		
		driver.switchTo().frame("iframeResult");
		
		driver.findElementByXPath("//button[contains(text(),'Try it')]").click();
		Alert ar=driver.switchTo().alert();
		ar.sendKeys("Smitha");
		ar.accept();
		
	String text =driver.findElementByXPath("//*[@id='demo']").getText();
	
	if(text.contains("Smitha"))
	{
		System.out.println("Matching");
	}else
	{
		System.out.println("Not matching");
	}
		
		
	}

}
