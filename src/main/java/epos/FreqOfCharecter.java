package epos;

import java.util.HashMap;

public class FreqOfCharecter {
	
	public static void charCount(String str) {
		
		
		HashMap<Character,Integer> charCountie=new HashMap<Character,Integer>(); 
		
		char[] ch=str.toCharArray();
		
		for (char c : ch) {
			if(charCountie.containsKey(c))
			{
				charCountie.put(c,charCountie.get(c)+1 );
			}else 
			{
				charCountie.put(c, 1);
			}
			
		}
		System.out.print(str+ " "+charCountie);
		
		

	}

	public static void main(String[] args) {
		
		String str="Hello";
		
		charCount(str);
	}

}
