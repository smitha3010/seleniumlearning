package epos;

import java.awt.Robot;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EposValuePacGtQuoteFirstParty {

	public static void main(String[] args) throws InterruptedException {
		Robot robot = null;
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://eposqa.candelalabs.io/");
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		driver.findElementByXPath("//span[text()='Continue to login']").click();
		driver.findElementByXPath("//input[@id='username']").sendKeys("t005@test.com.uat");
		driver.findElementByXPath("//input[@id='password']").sendKeys("Candela@005");
		driver.findElementByXPath("//input[@id='Login']").click();

		Thread.sleep(3000);

		System.out.println("The title is : " + driver.getTitle());

		if (driver.getTitle().contains("AXAAFFIN - FES")) {
			System.out.println("Login sucessfull");
		} else {
			System.out.println("Login Unsucessfull");
		}
        
		Thread.sleep(3000);
		WebElement ele = driver.findElement(By.xpath("//span[contains(text(),'Get Quote')]"));
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].click()", ele);
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElementByXPath("//span[contains(text(),'ValuePac')]").click();
		
		driver.findElementByXPath("//span[contains(text(),'Next')]").click();
		
		driver.findElementByXPath("//button[@id='VAPC_proposer_cust_title_button']").click();
		
		driver.findElementByXPath("//li[@id='VAPC_proposer_cust_title_dropbox_item0']").click();
		driver.findElementByXPath("//input[@id='VAPC_proposer_cust_fname_input']").sendKeys("ValuePac Firstparty AM");
		driver.findElementByXPath("//input[@id='VAPC_proposer_cust_lname_input']").sendKeys("ValuePac Lastname");
		driver.findElementByXPath("//input[@placeholder='XXXXXX-XX-XXXX']").click();
		driver.findElementByXPath("//input[@placeholder='XXXXXX-XX-XXXX']").sendKeys("961111111111");
		
		
		


	}

}
