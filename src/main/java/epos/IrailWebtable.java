package epos;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class IrailWebtable {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();

		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();

		driver.get("https://erail.in/trains-between-stations/new-delhi-NDLS/mumbai-central-BCT");
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		List<WebElement> allRows = driver
				.findElementsByXPath("//table[@class='DataTable TrainList TrainListHeader']//tr");
		int rowCount = allRows.size();
		for (int i = 2; i<rowCount; i++) {

			int colCount = driver
					.findElementsByXPath("//table[@class='DataTable TrainList TrainListHeader']//tr[" + i + "]//td")
					.size();

			for (int j = 1; j <= colCount; j++) {

				String str = driver
						.findElementByXPath(
								"//table[@class='DataTable TrainList TrainListHeader']//tr[" + i + "]//td[" + j + "]")
						.getText();
				System.out.println(str);

			}

		}

	}

}
