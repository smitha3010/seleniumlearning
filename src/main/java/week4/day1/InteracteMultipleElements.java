package week4.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class InteracteMultipleElements {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		// WebDriverManager.firefoxdriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://leaftaps.com/opentaps/control/main");

		driver.manage().window().maximize();

		List<WebElement> elements = driver.findElementsByXPath("//input[@class='inputLogin']");
//			WebElement firstElement=elements.get(0);
//			WebElement SecondElement=elements.get(1);
//			firstElement.sendKeys("demosalesmanager");
//			SecondElement.sendKeys("crmsfa");
        //using for each loop
		for (WebElement eachElement : elements) {

			String name = eachElement.getAttribute("name");
			System.out.println(name);
		}
		
		//using for loop

		for (int i = 0; i < elements.size(); i++) {
			String name = elements.get(i).getAttribute("name");
			System.out.println(name);

		}

	}

}
