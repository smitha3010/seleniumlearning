package week4.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SelectAllCheckboxes {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		// WebDriverManager.firefoxdriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://leafground.com/pages/checkbox.html");

		driver.manage().window().maximize();
		
		List<WebElement> allCheckboxes=driver.findElementsByXPath("//label[text()='Select all below checkboxes ']/following::input");
		
		//click all the checkboxes
		for (WebElement eachElement : allCheckboxes) {
			eachElement.click();
		
		}
		
		//select only first three checkboxes
		for (int i = 0; i < allCheckboxes.size()-1; i++) {
			
			allCheckboxes.get(i).click();
			
			
		}
		
		

	}

}
