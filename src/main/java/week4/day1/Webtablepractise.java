package week4.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Webtablepractise {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://leafground.com/pages/table.html");
		
		List<WebElement> allRows = driver.findElementsByXPath("//table[@id='table_id']//tr");
		
		int rowCount = allRows.size();
		
		for (int i = 2; i < rowCount; i++) {
			
			List<WebElement> allData = driver.findElementsByXPath("//table[@id='table_id']//tr["+i+"]//td");
			int dataCount = allData.size();
			
			for (int j = 1; j <dataCount; j++) {
				
				String value = driver.findElementByXPath("//table[@id='table_id']//tr["+i+"]//td["+j+"]").getText();
				System.out.println(value);
	
	           if(value.equals("Gopi"))
	           {
	        	   System.out.println("Row number: "+i);
	        	   System.out.println("Col count: "+j);
	           }
				
			}
			
			
			
		
			
		}
		
	}

}
