package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BasicAuthentication {

	public static void main(String[] args) {
WebDriverManager.chromedriver().setup();
		
		//to disable browser notification
		ChromeOptions options = new ChromeOptions();		
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		
		// http://username:password@url
		driver.get("http://admin:admin@the-internet.herokuapp.com/basic_auth");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);


	}

}
