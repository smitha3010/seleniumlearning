package week4.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebTable {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		// WebDriverManager.firefoxdriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://leafground.com/pages/table.html");

		driver.manage().window().maximize();
		List<WebElement> allRows=driver.findElementsByXPath("//table[@id='table_id']//tr");
		int rowCount=allRows.size();
		
//		List<WebElement> allData=driver.findElementsByXPath("//table[@id='table_id']//tr[2]/td");
//	
//		int dataCount=allData.size();

		for (int i = 2; i <= rowCount; i++) {//rows
			
			List<WebElement> allData=driver.findElementsByXPath("//table[@id='table_id']//tr["+i+"]/td");
			
			int dataCount=allData.size();
			
			for (int j = 1; j <= dataCount; j++) {//columns
				
				String str = driver.findElementByXPath("//table[@id='table_id']//tr[" + i + "]/td["+j+"]").getText();//this is called dynamic xpath
				System.out.println(str);
				
				if(str.equals("Gopi")) {
					System.out.println("Row number is: "+i);
					System.out.println("Row col number is: "+j);
				}
			}
		
		}

//		String str=driver.findElementByXPath("//table[@id='table_id']//tr[2]/td[1]").getText();
//		System.out.println(str);
//		

	}

}
