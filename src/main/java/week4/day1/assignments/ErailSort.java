package week4.day1.assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ErailSort {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		// WebDriverManager.firefoxdriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://erail.in/");

		driver.manage().window().maximize();

		driver.findElementByXPath("//input[@id='txtStationFrom']").clear();
		driver.findElementByXPath("//input[@id='txtStationFrom']").sendKeys("ms");
		driver.findElementByXPath("//input[@id='txtStationFrom']").sendKeys(Keys.TAB);

		driver.findElementByXPath("//input[@id='txtStationTo']").clear();
		driver.findElementByXPath("//input[@id='txtStationTo']").sendKeys("mdu");
		driver.findElementByXPath("//input[@id='txtStationTo']").sendKeys(Keys.TAB);

		driver.findElementByXPath("//input[@id='chkSelectDateOnly']").click();
		
	List<WebElement> allRows=driver.findElementsByXPath("//table[@class='DataTable TrainList TrainListHeader']//tr");
		int rowCount=allRows.size();
		
		List<String> listTrainNames=new ArrayList<String>();

		
		for (int i = 1; i <=rowCount ; i++) {
		
		List<WebElement> allData = driver.findElementsByXPath("//table[@class='DataTable TrainList TrainListHeader']//tr["+i+"]/td[2]");

		int dataCount = allData.size();
		
		for (int j = 1; j <= dataCount; j++) {
		
		String trainName = driver.findElementByXPath("//table[@class='DataTable TrainList TrainListHeader']//tr[" + i + "]/td[2]")
				.getText();// this is called dynamic xpath
		listTrainNames.add(trainName);

	}

}
		Collections.sort(listTrainNames);
		System.out.println(listTrainNames);
}
}
