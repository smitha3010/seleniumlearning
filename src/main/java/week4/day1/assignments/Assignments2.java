package week4.day1.assignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Assignments2 {

	public static void main(String[] args) {
		
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://leafground.com/pages/Calendar.html");
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@id='datepicker']").click();
		driver.findElementByXPath("//table[@class='ui-datepicker-calendar']//tr[3]/td[2]").click();

	}

}
