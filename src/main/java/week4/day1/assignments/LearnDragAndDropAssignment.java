package week4.day1.assignments;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearnDragAndDropAssignment {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		RemoteWebDriver driver = new ChromeDriver();
		driver.get("https://jqueryui.com/droppable/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Thread.sleep(3000);
		
		
		WebElement source = driver.findElementByXPath("//p[text()='Drag me to my target']");

		WebElement target = driver.findElementByXPath("//div[@id='droppable']");

		// Actions --> Advanced user interactions
		Actions builder = new Actions(driver);
		builder.dragAndDrop(source, target).perform();
		//builder.dragAndDropBy(source, 100, 100).perform();
	}

}
