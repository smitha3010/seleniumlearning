package week4.day1.assignments;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Assignment1 {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://leafground.com/pages/table.html");
		driver.manage().window().maximize();
		
        //To get the Row count 
		WebElement rowCount = driver.findElementByXPath("//table[@id='table_id']/tbody");
		List<WebElement> TotalRowList = rowCount.findElements(By.tagName("tr"));
		System.out.println("Total number of rows in the table are :" + TotalRowList.size());
		
		//To get the column count
		WebElement colCount = driver.findElementByXPath("//table[@id='table_id']/tbody/tr[2]");
		List<WebElement> TotalColList = colCount.findElements(By.tagName("td"));
		System.out.println("Total number of Column in the table are :" + TotalColList.size());
		
		//Get the progress value of Learn to interact with elements
		WebElement text=driver.findElementByXPath("(//tr[@class='even']/td)[2]");
		System.out.println(text.getText());
				
		driver.findElementByXPath("(//input[@type='checkbox'])[3]").click();
		
		

	}

}
