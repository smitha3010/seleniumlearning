package week4.day2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class MultipleWindows {

	public static void main(String[] args) {
		// new tab and new window both are same

		WebDriverManager.chromedriver().setup();
		// WebDriverManager.firefoxdriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://leafground.com/pages/Window.html");

		driver.manage().window().maximize();

		String str = driver.getTitle();
		System.out.println(str);

//		String str1 = driver.getWindowHandle();
//		System.out.println(str1);

		driver.findElementByXPath("//button[@id='home']").click();
		
		Set<String> windowhandles=driver.getWindowHandles();
		
		for (String eachref : windowhandles) {
			System.out.println(eachref);
			
		}
		//if we want to get single value then convert to list 
		
		List<String> listhandles=new ArrayList<String>(windowhandles);
     	String secWin=listhandles.get(1);
		//System.out.println(listhandles.get(1));
		//To move my control from one window to another window
		driver.switchTo().window(secWin);
		String title1=driver.getTitle();
		System.out.println(title1);
		driver.findElementByXPath("//img[@alt='Buttons']").click();
		String title2=driver.getTitle();
		System.out.println(title2);
		
		driver.close();//to close the current window 
		//driver.quit();//to close all the windows
		driver.switchTo().window(listhandles.get(0));
		
		
//		String smi=driver.getTitle();
//		System.out.println(smi);

	}
	
	//To move my control from one window to another window


}
