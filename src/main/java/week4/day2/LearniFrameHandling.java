package week4.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearniFrameHandling {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		// WebDriverManager.firefoxdriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://dev111627.service-now.com/");

		driver.manage().window().maximize();
		
		//control will be moved from main window to frame
		//driver.switchTo().frame("gsft_main");
		
		//using index
		//driver.switchTo().frame(0);
		
		WebElement myelement=driver.findElementByXPath("//iframe[@title='Main Content']");
	   driver.switchTo().frame(myelement);
		
		//interact with the element inside the frame
		driver.findElementById("user_name").sendKeys("admin");
		
		//to switch the control to the main window from frame
		
		
		String str=driver.findElementByXPath("//div[text()='About ServiceNow']").getText();
		System.out.println(str);
		
		
		

	}

}
