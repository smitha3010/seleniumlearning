package week4.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearNestedFrame {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		// WebDriverManager.firefoxdriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");

		driver.manage().window().maximize();
		
		driver.switchTo().frame("iframeResult");
		//driver.findElementById("Click").click();
		
		driver.findElementByXPath("//button[text()='Try it']").click();
		Alert alert1=driver.switchTo().alert();
		alert1.sendKeys("Smitha");
		alert1.accept();
		
		
	String str=driver.findElementByXPath("//p[@id='demo']").getText();
	
	if(str.contains("Smitha")) {
		System.out.println("Smitha is present in the given sentence");
	}else
		System.out.println("Smitha is not present in the given sentence");
		
		
		
//		driver.switchTo().defaultContent();
//		
//		driver.switchTo().frame(1);//outer frame
//		driver.switchTo().frame("frame2");//inner frame
//		driver.findElementById("Click1").click();
//		
//		//driver.switchTo().parentFrame();
//		//driver.switchTo().parentFrame();
//		//String str=driver.findElementByTagName("h1").getText();
//	//	System.out.println(str);
	}

}
