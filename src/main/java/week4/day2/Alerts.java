package week4.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Alerts {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		// WebDriverManager.firefoxdriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://leafground.com/pages/Alert.html");

		driver.manage().window().maximize();
		
		
		//driver.findElementByXPath("//button[text()='Alert Box']").click();

		// to switch the control from main window to alert
		Alert alert = driver.switchTo().alert();
		// to get the text
		String text = alert.getText();
		System.out.println(text);
		// to click on OK button
		alert.accept();
//
//		// unless we handle the alerst we cannot perform anything we will get unhandled
//		// alert exception
//		driver.findElementByXPath("//button[text()='Confirm Box']").click();
//
		Alert alert2 = driver.switchTo().alert();
//
//		String text2 = alert2.getText();
//		System.out.println(text2);
//		// alert2.accept();
//		alert2.dismiss();// to candel the alert
//
//		// Click on prompty alert
//		// Prompt Box
//		driver.findElementByXPath("//button[text()='Prompt Box']").click();
//		Alert alert3 = driver.switchTo().alert();
//
//		String text3 = alert3.getText();
//		System.out.println(text3);
//
//		alert3.sendKeys("Smitha");
//		Thread.sleep(3000);
//		alert3.accept();
//		// alert3.dismiss();
		
		driver.findElementByXPath("//button[text()='Sweet Alert']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//button[contains(text(),'OK')]").click();

	}

}
