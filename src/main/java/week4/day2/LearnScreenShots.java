package week4.day2;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LearnScreenShots {

	public static void main(String[] args) throws IOException {
		WebDriverManager.chromedriver().setup();
		// WebDriverManager.firefoxdriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("http://leafground.com/pages/Window.html");

		driver.manage().window().maximize();
		
		WebElement eleHome=driver.findElement(By.id("home"));
		File source=driver.getScreenshotAs(OutputType.FILE);
		File target=new File("./snaps/Home.png");
		FileUtils.copyFile(source, target);
		
		//to take the screenshot of window
//		File source=driver.getScreenshotAs(OutputType.FILE);
//		
//		//./represents route folder of my project
//		File target=new File("./snaps/LeafGround.png");
//		
//		//copy the source file into target file
//		FileUtils.copyFile(source, target);

	}

	
}
