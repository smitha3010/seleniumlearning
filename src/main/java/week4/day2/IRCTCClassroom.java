package week4.day2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class IRCTCClassroom {

	public static void main(String[] args) throws IOException, InterruptedException {
		WebDriverManager.chromedriver().setup();

		// to disable browser notification
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);

		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(3000);
//		Alert alr = driver.switchTo().alert();
//		alr.accept();
		driver.findElementByXPath("//button[contains(text(),'OK')]").click();
		driver.findElementByXPath("//a[contains(text(),'FLIGHTS')]").click();
		
		
		Set<String> windowhandles = driver.getWindowHandles();

		for (String eachref : windowhandles) {
			System.out.println(eachref);

		}
		List<String> listhandles = new ArrayList<String>(windowhandles);
		String secWin = listhandles.get(1);

		driver.switchTo().window(secWin);
		driver.findElementByXPath("//button[contains(text(),'Ok')]").click();

		File source = driver.getScreenshotAs(OutputType.FILE);
		File target = new File("./snaps/flights.png");
		FileUtils.copyFile(source, target);

		driver.close();

	}

}
