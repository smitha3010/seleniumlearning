package week5.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public String[][] excelData(String FileName) throws IOException {

		// Step1:Setup the Workbook path

		XSSFWorkbook wb = new XSSFWorkbook("./data/"+FileName+".xlsx");

		// step2: get into the worksheet

		XSSFSheet ws = wb.getSheet("Sheet1");

		int rows = ws.getLastRowNum();
		System.out.println("Last row which is having data: "+rows);// it is excluding the heading

//		int rows1 = ws.getPhysicalNumberOfRows();
//		System.out.println(rows1);//it will include the heading

		// to get the number of cells in a row
		int cells = ws.getRow(0).getLastCellNum();
		System.out.println("The number of cells is: "+cells);
		
		//declare 2D array 
		
		String[][] data=new String[rows][cells];

		for (int i = 1; i <= rows; i++) {

			for (int j = 0; j < cells; j++) {
				// This is hardcoded so everytime we change in excel we shud keep on changing
				// the row
				// so count the row programmatically
				String value = ws.getRow(i).getCell(j).getStringCellValue();
				System.out.println(value);
				
				data[i-1][j]=value;

			}

			// This is hardcoded so everytime we change in excel we shud keep on changing
			// the row
			// so count the row programmatically
//			String value = ws.getRow(i).getCell(0).getStringCellValue();
//			System.out.println(value);

//			//step3: get into the row
//			
//			XSSFRow row = ws.getRow(i);
//			
//			//step4:to get into the cell
//			
//			XSSFCell cell = row.getCell(0);
//			
//			//to read the data from a cell
//			
//			String value = cell.getStringCellValue();
//			System.out.println(value);

		}

		// To close the workbook

		wb.close();
		return data;

	}

}
