package Amazon;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Amazon {

	@Test
	public void login() {

		WebDriverManager.chromedriver().setup();

		ChromeDriver driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.get("https://www.amazon.in/");

		WebElement ele = driver.findElementByXPath("//span[@class='icp-nav-flag icp-nav-flag-in']");

		Actions lang = new Actions(driver);

		lang.moveToElement(ele).build().perform();

		driver.findElementByXPath("(//i[@class='icp-radio'])[4]").click();

		String namskara = driver.findElementByXPath("//span[@id='glow-ingress-line1']").getText();
		
		//System.out.println(namskara);

		Assert.assertEquals(namskara, driver.findElementByXPath("//span[@id='glow-ingress-line1']").getText());
//		driver.close();
	}

	
}
