package Amazon;


import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class HardAssert {

	@Test

	public void loginTest() throws IOException {
		
		WebDriverManager.chromedriver().setup();
		
		ChromeDriver driver=new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		WebElement username=driver.findElementByXPath("(//input[@class='inputLogin'])[1]");
		
		WebElement password=driver.findElementByXPath("(//input[@class='inputLogin'])[2]");
		
		Assert.assertTrue(username.isDisplayed());
		username.sendKeys("DemoSalesManager");
		
		Assert.assertTrue(password.isDisplayed());
		password.sendKeys("crmsfa");
		
		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		
		String ExpTitle = driver.findElementByXPath("//h2").getText();
		
		System.out.println(ExpTitle);

		Assert.assertEquals( ExpTitle,driver.findElementByXPath("//h2").getText() );
		
		
		File src=driver.getScreenshotAs(OutputType.FILE);
		
		File target = new File("./snaps/Testleaf.png");
		
		FileUtils.copyFile(src, target);
		
		
		
		

	}

}
