package Amazon;


import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNg {
	
	
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Before Method");
	}
	
	@BeforeSuite
	public void bS() {
		System.out.println("Before Suite");
	}
	@BeforeClass
	public void bC() {
		System.out.println("Before class");
	}
    @BeforeTest
    public void bT() {
    	System.out.println("Before Test");
    }
	
	@Test
	public void beforeTest1() {
		System.out.println("Test1");
		
		
	}

	@Test
	public void beforeTest2() {
		System.out.println("Test2");
		
		
	}
	
	
	
	 @AfterTest
	    public void aT() {
	    	System.out.println("after Test");
	    }
	 @AfterSuite
	    public void aS() {
	    	System.out.println("after suite");
	    }
	 @AfterMethod
	    public void aM() {
	    	System.out.println("after method");
	    }
	 @AfterClass
	    public void aC() {
	    	System.out.println("after class");
	    }
}
