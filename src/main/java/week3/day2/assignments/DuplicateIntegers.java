package week3.day2.assignments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
public class DuplicateIntegers {
   public static void main(String[] args) {
      ArrayList<Integer> arr = new ArrayList<Integer>(Arrays.asList(1,3,8,3,11,5,6,4,7,6,7));
      
      for(int i=0;i<arr.size();i++) {
    	  if (arr.contains(arr.get(i))){
    		  System.out.println(arr.get(i)+"is duplicated");
    	  }
      }
//      HashSet<Integer> h1 = new HashSet<>();
//      HashSet<Integer> h2 = new HashSet<>();
//      for (Integer integer : arr) {
//         if(!h1.add(integer)) {
//            h2.add(integer);
//         }
//      }
//      System.out.println("Duplicate integers in given list is/are " + h2);
   }
}

