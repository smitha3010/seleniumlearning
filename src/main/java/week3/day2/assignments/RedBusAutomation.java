package week3.day2.assignments;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class RedBusAutomation {

	public static void main(String[] args) throws InterruptedException {
		WebDriverManager.chromedriver().setup();

		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.redbus.in/");

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		System.out.println(driver.getTitle());

		WebElement source = driver.findElementByXPath("//input[@data-message='Please enter a source city']");
		source.sendKeys("C");
		Thread.sleep(2000);
		source.sendKeys(Keys.ARROW_DOWN);
		source.sendKeys(Keys.ENTER);

		WebElement destination = driver.findElementByXPath("//input[@data-message='Please enter a destination city']");
		destination.sendKeys("Chennai");
		Thread.sleep(2000);
		destination.sendKeys(Keys.TAB);

		driver.findElementByXPath("//input[@id='onward_cal']").click();

		driver.findElementByXPath("//td[@class='next']/button").click();
		driver.findElementByXPath("//td[text()='8']").click();

		driver.findElementByXPath("//button[text()='Search Buses']").click();

		WebElement buses = driver.findElementByXPath("//span[@class='f-bold busFound']");
		String text = buses.getText();
		System.out.println("No of the buses found :" + text);

		Thread.sleep(1000);

		driver.findElementByXPath("//i[@class='icon icon-close c-fs']").click();
		Thread.sleep(1000);

		driver.findElementByXPath("//label[@title='SLEEPER']").click();
		WebElement SleeperBuses = driver.findElementByXPath("//span[@class='f-bold busFound']");
		String text1 = SleeperBuses.getText();
		System.out.println("No of Sleeper buses found :" + text1);

		Thread.sleep(1000);

		driver.findElementByXPath("//label[@title='AC']").click();
		WebElement ACBuses = driver.findElementByXPath("//span[@class='f-bold busFound']");
		String text2 = buses.getText();
		System.out.println("No of AC buses found :" + text2);

	}

}
