package week3.day2.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class LearnSet {
	
	
	public static void main(String[] args) {
		
		Set<String> values=new LinkedHashSet<String>();
	    values.add("Apple");
		values.add("Ball");
		values.add("Cat");
		values.add("elephant");
		//System.out.println(values.add("Apple"));
		
		//copy data from one collection to another collection 
		
		List<String> listvalues=new ArrayList<String>(values);
		
		System.out.println(listvalues.get(2));
		
		
		
//		
//		for (String eachValue : values) {
//			System.out.println(eachValue);
//			
//		}
//		

	}

}
