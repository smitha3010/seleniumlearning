package week3.day2.collections;

import java.util.HashSet;
import java.util.Set;

public class RemoveDup {

	public static void main(String[] args) {

		String str = "PayPal India";

		char[] ch = str.toCharArray();

		Set<Character> c = new HashSet<Character>();

		for (int i = 0; i < ch.length; i++) {

			if (!c.contains(ch[i])) {

				c.add(ch[i]);

				if (ch[i] != ' ') {
					System.out.print(ch[i]);
				}

			}

		}
	}

}
