package week3.day2.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		//List<Integer> values = new ArrayList();
		List<String> values = new ArrayList();

		// method to add the values to the list

//		values.add(100);
//		values.add(200);
//		values.add(300);
//		values.add(400);
//		values.add(500);
		//values.add("athartha");
		values.add("smitha");
		values.add("Gautam");
		values.add("Deeksha");
		// values.add(true);
		// values.add('A');
		// values.add(20.25);

		// to find the number of items in list
//		System.out.println(values.size());
//		//values.add(600);
//		System.out.println(values.size());
//		
	//	values.remove(3);
		//values.remove(200);

//		System.out.println("+++++++++++++++++++++++++++++++++++++++");
//
//		// to get the first data
//		System.out.println(values.get(0));
//		System.out.println("+++++++++++++++++++++++++++++++++++++++");
//
//		System.out.println(values.get(values.size() - 1));
//		System.out.println("+++++++++++++++++++++++++++++++++++++++");
//
//		// to get all the elements
//
//		for (int i = 0; i < values.size(); i++) {
//			System.out.println(values.get(i));
//
//		}
//		System.out.println("+++++++++++++++++++++++++++++++++++++++");
//
//		// restrict for a single data type
//
//		// using foreach
//		// datatype tempvariable : source
//		System.out.println("Printing using foreach");
//		for (String eachvalue : values) {
//
//			System.out.println(eachvalue);
//
//		}
//		
		System.out.println("=============================================");
		
		//collections is a class
		Collections.sort(values);
		System.out.println(values);
		
//		for ( int i = 0; i < values.size(); i++) {
//			System.out.println(values.get(i));
//			
//		}

	}

}
