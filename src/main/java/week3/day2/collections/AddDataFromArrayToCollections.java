package week3.day2.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AddDataFromArrayToCollections {

	public static void main(String[] args) {

		String[] values = { "HCL", "Wipro", "Aspire Systems", "CTS" };
//		
	List<String> listvalues = new ArrayList<String>();
//		
//		int[] values1= {10,20,30};
//		List<Integer> listvalues1=new ArrayList<Integer>(values1.length);
//		for(int i:values1)
//		{
//			listvalues1.add(i);
//		}
//		
//		System.out.println(listvalues1);
		
		for (String eachvalue : values) {

			listvalues.add(eachvalue);

		}
		 System.out.println(listvalues);

		Collections.sort(listvalues, Collections.reverseOrder());
		System.out.println("Using Collections reverse" + listvalues);

		Collections.sort(listvalues);

		for (int i = listvalues.size() - 1; i >= 0; i--) {

			System.out.print(listvalues.get(i));
			System.out.print(",");

		}

	}

}
