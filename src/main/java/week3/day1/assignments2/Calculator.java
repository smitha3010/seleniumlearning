package week3.day1.assignments2;

public class Calculator {

	public void add(int num1, int num2) {

		int sum = num1 + num2;

		System.out.println("Sum using two integers :" + sum);

	}

	public void add(int num1, int num2, int num3) {

		int sum = num1 + num2 + num3;
		System.out.println("Sum using 3 integers: " + sum);

	}

	private void mul(int num1, int num2) {

		int mul = num1 * num2;
		System.out.println("Mul Using 2 intergers: " + mul);

	}

	public void mul(int num1, double num2) {
		double mul = num1 * num2;
		System.out.println("Mul Using 1 intergerand 1 double: " + mul);

	}

	public void subtract(int num1, int num2) {

		int sub = num1 - num2;
		System.out.println("Sub using 2 intergers:" + sub);

	}

	public void subtract(double num1, double num2) {

		double sub = num1 - num2;
		System.out.println("Sub using 2 double:" + sub);

	}

	private void divide(int num1, int num2) {

		int div = num1 / num2;
		System.out.println("div using 2 intergers:" + div);

	}

	public void divide(double num1, int num2) {

		double div = num1 / num2;
		System.out.println("div using 1 double and 1 int:" + div);

	}

	public static void main(String[] args) {
		Calculator cal = new Calculator();
		cal.add(10, 20);
		cal.add(10, 20, 30);
		cal.mul(20, 30);
		cal.mul(10, 20.2);
		cal.subtract(20.02, 22);
		cal.subtract(20, 10);
		cal.divide(400, 20);
		cal.divide(20.2, 2);

	}

}
