package week3.day1;

public class CarClass extends Vehicle {

	public void handBreak(int sum) {
		System.out.println("Hand Break");

	}

	public void seatBelt() {
		System.out.println("Seat Belt");

	}

	public static void main(String[] args) {
		Vehicle veh=new Vehicle();
		veh.applyBrake(2);
		veh.soundHorn();
		
		
		CarClass c=new CarClass();
		c.handBreak(3);
		c.seatBelt();
		c.applyBrake(2);
		c.soundHorn();
	
		

	}
}
