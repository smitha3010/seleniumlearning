package week3.day1;

public class Audi extends CarClass {
	
	
	private void airBag() {
		System.out.println("air bag");

	}

	public static void main(String[] args) {
		
		Audi audiCar=new Audi();
		
		audiCar.airBag();
		audiCar.applyBrake(5);
		audiCar.soundHorn();
		audiCar.seatBelt();
		audiCar.handBreak(6);
		

	}

}
