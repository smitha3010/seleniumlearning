package week3.day1;

public class ConstructorEmployee {

	int empId;
	String empName;
	boolean empStatus;

	public ConstructorEmployee() {
		//this(200,"smith",true);
		System.out.println("Defualt Constructor");
	}

	ConstructorEmployee(int empId, String empName, boolean empStatus) {
		this();
		System.out.println("Parametrized constructor");
		this.empId = empId;
		this.empName = empName;
		this.empStatus = empStatus;

	}

	public static void main(String[] args) {

		// ClassName ObjectName=new Constructor();
		ConstructorEmployee empy = new ConstructorEmployee(200, "smitha", true);
		System.out.println(empy.empId);
		System.out.println(empy.empName);
		System.out.println(empy.empStatus);

	}

}
