package week3.day1classroom;

public class MyPhone  {

	public static void main(String[] args) {
		
		SmartPhone smart=new SmartPhone();
		smart.connectWhatsApp();
		smart.takeVideo();
		smart.sendMsg();
		smart.makeCall();
		smart.saveContact();

	}

}
