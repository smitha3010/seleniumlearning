package actitime;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Actitime_Task {
	public static void main(String[] args) {

		ChromeDriver oBrowser = null;
		Robot robot = null;
		String sText = null;

		try {
			WebDriverManager.chromedriver().setup();

			// login to actiTime->Import task-> Validate all the imported tasks
			// individually->Delete all the tasks one by one->Logout
			
			// (a) login to actiTime application

			oBrowser = new ChromeDriver();
			oBrowser.manage().window().maximize();
			oBrowser.get("http://localhost/login.do"); // Load or navigate the URL
			Thread.sleep(2000);

			oBrowser.findElement(By.name("username")).sendKeys("admin");
			oBrowser.findElement(By.name("pwd")).sendKeys("manager");
			oBrowser.findElement(By.id("loginButton")).click();
			Thread.sleep(2000);

			// close the shortcut window
			if (oBrowser.findElement(By.xpath("//div[@style='display: block;']")).isDisplayed()) {
				oBrowser.findElement(By.id("gettingStartedShortcutsMenuCloseId")).click();
			}

			else {
				return;
			}

			Thread.sleep(5000);
			// click on "Tasks" tab

			oBrowser.findElement(By.xpath("//div[@class='label'][text()='TASKS']")).click();

			// click on "add New Task" button
			oBrowser.findElement(By.xpath("//div[@class='title ellipsis'][text()='Add New Task']")).click();

			// click on "Import Tasks from CSV" option
			oBrowser.findElement(By.xpath("//div[@class='item importTasks ellipsis'][text()='Import tasks from CSV']"))
					.click();
			Thread.sleep(5000);
			// click inside the window to choose the file
			oBrowser.findElement(By.xpath("//div[@id='dropzoneClickableArea']")).click();
			Thread.sleep(5000);

			// browse and open the csv file from the PC location
			// Use the robot class
			// Copy the CSV file path
			StringSelection strPath = new StringSelection("D:\\OnlineClasses\\CoreJava\\Sample.csv");
			Clipboard clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipBoard.setContents(strPath, null);

			// PRess Ctrl + V
			robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			Thread.sleep(2000);
			robot.keyRelease(KeyEvent.VK_CONTROL);

			Thread.sleep(2000);

			robot.keyPress(KeyEvent.VK_ENTER);
			Thread.sleep(2000);
			robot.keyRelease(KeyEvent.VK_ENTER);

			// click on "Complete Import" button
			oBrowser.findElement(By.xpath("//div[@id='loadTasksFromCSVPopup_commitBtn']")).click();
			Thread.sleep(5000);

			// close the import task window
			oBrowser.findElement(By.xpath("//div[@id='closeLoadTasksFromCSVPopupButton']")).click();
			Thread.sleep(3000);
			// Validate all the imported tasks individualy

			sText = oBrowser.findElement(By.xpath("//div[text()='Task 3']")).getText();
			if (sText.equalsIgnoreCase("Task 3")) {
				System.out.println("Task 3 is added sucessfully");
			} else {
				System.out.println("Task 3 is not added sucessfully");
				return;
			}

			sText = oBrowser.findElement(By.xpath("//div[text()='Task 4']")).getText();
			if (sText.equalsIgnoreCase("Task 4")) {
				System.out.println("Task 4 is added sucessfully");
			} else {
				System.out.println("Task 4 is not added sucessfully");
				return;
			}

			sText = oBrowser.findElement(By.xpath("//div[text()='Task 1']")).getText();
			if (sText.equalsIgnoreCase("Task 1")) {
				System.out.println("Task 1 is added sucessfully");
			} else {
				System.out.println("Task 1 is not added sucessfully");
				return;
			}

			sText = oBrowser.findElement(By.xpath("//div[text()='Task 2']")).getText();
			if (sText.equalsIgnoreCase("Task 2")) {
				System.out.println("Task 2 is added sucessfully");
			} else {
				System.out.println("Task 2 is not added sucessfully");
				return;
			}

			// Delete all the tasks one by one

			// deleted the task 3

			oBrowser.findElement(By.xpath("//div[text()='Task 3']")).click();
			Thread.sleep(5000);
			oBrowser.findElement(By.xpath("//*[@id=\"taskListBlock\"]/div[3]/div[1]/div[2]/div[3]/div/div")).click();

			oBrowser.findElement(By.xpath("//*[@id=\"taskListBlock\"]/div[3]/div[4]/div/div[3]/div")).click();

			oBrowser.findElement(By.xpath("//*[@id=\"taskPanel_deleteConfirm_submitTitle\"]")).click();

			// deleted the task 4

			oBrowser.findElement(By.xpath("//div[text()='Task 4']")).click();
			Thread.sleep(5000);
			oBrowser.findElement(By.xpath("//*[@id=\"taskListBlock\"]/div[3]/div[1]/div[2]/div[3]/div/div/div[2]"))
					.click();

			oBrowser.findElement(By.xpath("//*[@id=\"taskListBlock\"]/div[3]/div[4]/div/div[3]/div")).click();

			oBrowser.findElement(By.xpath("//*[@id=\"taskPanel_deleteConfirm_submitTitle\"]")).click();

			// deleted the task 1

			oBrowser.findElement(By.xpath("//div[text()='Task 1']")).click();
			Thread.sleep(5000);
			oBrowser.findElement(By.xpath("//*[@id=\"taskListBlock\"]/div[3]/div[1]/div[2]/div[3]/div/div/div[2]"))
					.click();

			oBrowser.findElement(By.xpath("//*[@id=\"taskListBlock\"]/div[3]/div[4]/div/div[3]/div")).click();

			oBrowser.findElement(By.xpath("//*[@id=\"taskPanel_deleteConfirm_submitTitle\"]")).click();

			// deleted the task 2

			oBrowser.findElement(By.xpath("//div[text()='Task 2']")).click();
			Thread.sleep(5000);
			oBrowser.findElement(By.xpath("//*[@id=\"taskListBlock\"]/div[3]/div[1]/div[2]/div[3]/div/div/div[2]"))
					.click();

			oBrowser.findElement(By.xpath("//*[@id=\"taskListBlock\"]/div[3]/div[4]/div/div[3]/div")).click();

			oBrowser.findElement(By.xpath("//*[@id=\"taskPanel_deleteConfirm_submitTitle\"]")).click();

			// (j) logout from actiTime
			oBrowser.findElement(By.id("logoutLink")).click();
			Thread.sleep(2000);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			oBrowser = null;
		}

	}


}
