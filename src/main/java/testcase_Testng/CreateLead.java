package testcase_Testng;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import week5.day2.ReadExcel;

public class CreateLead extends BaseClass{

	@Test(dataProvider ="sendData")
	public void runCreateLead(String company,String FN,String LN,String PhNo) {
		
		//driver.findElement(By.linkText("Leads")).click();
		driver.findElement(By.linkText("Create Lead")).click();
		driver.findElement(By.id("createLeadForm_companyName")).sendKeys(company);
		driver.findElement(By.id("createLeadForm_firstName")).sendKeys(FN);
		driver.findElement(By.id("createLeadForm_lastName")).sendKeys(LN);
		driver.findElement(By.id("createLeadForm_primaryPhoneNumber")).sendKeys(PhNo);
		driver.findElement(By.name("submitButton")).click();
		
	}
	
	@DataProvider(indices =0)
	public String[][] sendData() throws IOException {
		
		ReadExcel re=new ReadExcel();
		String[][] data=re.excelData("CreateLead");
		
		return data;
		
//		String[][] data = new String[2][4];
//		data[0][0] = "TestLeaf";
//		data[0][1] = "Smitha";
//		data[0][2] = "Smitham";
//		data[0][3] = "99";
//		
//		data[1][0] = "TestLeaf";
//		data[1][1] = "Smi";
//		data[1][2] = "Smithu";
//		data[1][3] = "97";
//				
		
		
	}
}
