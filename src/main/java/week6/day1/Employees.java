package week6.day1;

public class Employees {
	String empName;
	int empId;
	static String companyName;

	Employees(String name, int id, String company) {
		empName = name;
		empId = id;
		companyName = company;

	}
	
	private void printDetails() {
		System.out.println(empName + " " + empId + " " + companyName);

	}
	public static void companyAddress() {
		System.out.println("MG Road,Banglore");
	}

	public static void main(String[] args) {

		Employees emp1 = new Employees("Smitha", 100, "TestLeaf");
		Employees emp2 = new Employees("gautam", 120, "Testleaf software solutions");
		emp1.printDetails();
		emp2.printDetails();
		
		companyAddress();

	}

}
