package week6.day2;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class ReRunFailedTest implements IRetryAnalyzer{
	
	int maxRetry = 3;
	int retryCount = 0;//initial count 

	@Override
	public boolean retry(ITestResult result) {
		//failed && 0 < 3
		if(!result.isSuccess() && retryCount < maxRetry) {
			retryCount++;
			return true;
	}
		return false;
	}

}
