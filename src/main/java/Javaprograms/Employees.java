package Javaprograms;

public class Employees {

	String name;// instance variables

	int age;

	public Employees(String name, int age) {

		this.name = name;
		this.age = age;

		System.out.println(name + " " + age);
		System.out.println("Inside Constructor");
	}
	public Employees( int age,String name) {

		this.name = name;
		this.age = age;

		System.out.println(name + " " + age);
		System.out.println("Inside Constructor");
	}

	public void smitha(String name, int age) {

		this.name = name;
		this.age = age;
		System.out.println("Hi Smitha");
		System.out.println(name + " " + age);

	}

	public static void main(String[] args) {

		Employees e1 = new Employees("gautam", 25);
		

		e1.smitha("smitha", 25);

	}

}
