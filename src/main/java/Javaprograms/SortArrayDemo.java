package Javaprograms;

import java.util.Arrays;

public class SortArrayDemo {

	// Bubblesort

	public static void main(String[] args) {

		int a[] = { 2, 6, 1, 4, 9 };// compare the first index with all the others
	
		int temp;
		for (int i = 0; i < 5; i++) {
			for (int j = i + 1; j < 5; j++) {
				if (a[i] > a[j]) {
					// swap
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;

				}
			}
		}

		
		
		System.out.println("Ascending order is ");
		for (int i = 0; i < 5; i++) {
			System.out.println(a[i]);
		}
		
		
		System.out.println("=========================");
		
		System.out.println("Decending order is ");

		for(int i =a.length-1;i>=0;i--) {
			System.out.println(a[i]);
		}

	}

}
