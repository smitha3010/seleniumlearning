package Javaprograms;

public class SwapWithoutUsingTemp {

	public static void main(String[] args) {

		// swap without variable
		int a = 4;
		int b = 5;

		a = a + b;// 9
		b = a - b;// 5

		a = a - b;// 4

		// swap with temp variable
//		int a =4;
//		int b=5;
//		int temp;
//		
//		temp = a;
//		a=b;
//		b=temp;
//		
//		System.out.println("b= "+b );
//		System.out.println("a= "+a);

	}

}
