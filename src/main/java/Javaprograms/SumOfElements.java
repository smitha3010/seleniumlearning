package Javaprograms;

public class SumOfElements {

	public static void main(String[] args) {

		int[] a = { 1, 3, 6, 7, 8 };

		int sum = SumArray(a);
		System.out.println(sum);

	}

	public static int SumArray(int[] a) {

		int sum = 0;

		for (int i = 0; i < a.length; i++) {

			// System.out.println(a[i]);
			sum = sum + a[i];
		}

		return sum;

	}

}
