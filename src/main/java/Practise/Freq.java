package Practise;

import java.util.HashMap;

public class Freq {

	public static void countChar(String s) {

		HashMap<Character, Integer> charCount = new HashMap<Character, Integer>();

		char[] ch = s.toCharArray();
		
		for (char c : ch) {
			
			if(charCount.containsKey(c))
			{
				charCount.put(c,charCount.get(c)+1 );
			}else
			{
				charCount.put(c,1);
			}
			
			
			
		}
		System.out.println(s+" "+charCount);

	}

	public static void main(String[] args) {

		String s = "Hello";
		countChar(s);

	}

}
