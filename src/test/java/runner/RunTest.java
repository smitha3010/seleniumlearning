package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(features = "src/test/java/features", glue = { "steps","Hooks"
		 }, monochrome = true, publish = true // to execute all the @functional scenarios
//tags="@smoke or @functional"//to execute all the test cases with @smoke or regression
//tags="@smoke and @regression"//to execute both smoke and regression
//tags="not @functional"
)
public class RunTest extends AbstractTestNGCucumberTests {

}
