package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateLead extends BaseClass {
//	public ChromeDriver driver;
//
//	@Given("Open the chrome browser")
//	public void openBrowser() {
//		WebDriverManager.chromedriver().setup();
//		driver = new ChromeDriver();
//		driver.manage().window().maximize();
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//	}

//	@And("Load the application url {string}")
//	public void loadUrl(String url) {
//		driver.get(url);

//	}

	@And("Enter the usename as {string}")
	public void enterUsername(String username) {
		driver.findElementByXPath("//input[@id='username']").sendKeys(username);

	}

	@And("Enter the pasword as {string}")
	public void enterPassword(String password) {
		driver.findElementByXPath("//input[@id='password']").sendKeys(password);

	}

	@When("Click on Logn button")
	public void click_on_login_button() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Then("Homepage shoud be displayed")
	public void verifyHomePage() {
		System.out.println("Home page is displayed");

	}

	@When("Click on crmsa link")
	public void click_on_crmsfa_link() {
		driver.findElement(By.linkText("CRM/SFA")).click();
	}

//	@When("Click on crmsfa link")
//	public void clickCrmsfaLink() {
//		driver.findElement(By.linkText("CRM/SFA")).click();

}
