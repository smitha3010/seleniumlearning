package Hooks;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import steps.BaseClass;

public class Hooksimplementation extends BaseClass {
	
	
	@Before
	public void preCondition() {
		WebDriverManager.chromedriver().setup();
		 driver = new ChromeDriver();
		 driver.manage().window().maximize();
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 driver.get("http://leaftaps.com/opentaps/control/main");
	}
	
	@After
	public void postConditon() {
		
		driver.close();
		
	}

}
