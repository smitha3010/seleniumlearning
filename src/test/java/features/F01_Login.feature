Feature: Login functionality of LeafTaps application

#Background:
#Given Open the chrome browser
#And Load the application url 'http://leaftaps.com/opentaps/control/main'

@smoke 
Scenario Outline: TC001_Login with positive data
Given Enter the username as <username>
And Enter the password as <password>
And Click on Login Button 
Then Homepage should be displayed

Examples:
|username|password|
|'Demosalesmanger'|'crmsfa'|
|'DemoCSR'|'crmsfa'|


Scenario Outline: TC002_Login with negative data
Given Enter the username as <username>
And Enter the password as <password>
And Click on Login Button 
#But Error message should be displayed

Examples:
|username|password|
|'Demosalesmanger'|'crmsfa123'|
|'Demo'|'crmsfa'|
